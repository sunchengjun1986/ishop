

#import "PersonalCenterTableViewCell.h"

@implementation PersonalCenterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *viewBg = [[UIView alloc]initWithFrame:CGRectMake(kWidth(8), 0, kWidth(304),kHeight(50))];
        viewBg.backgroundColor = kWhiteColor;
        [self addSubview:viewBg];
        
        self.backgroundColor = UIColorFromRGB(0xf3f3f3);
        
        self.imageViewIcon = [[UIImageView alloc]init];
        self.imageViewIcon.frame = CGRectMake(kWidth(27), kHeight(15), kWidth(23), kHeight(23));
        [self addSubview:self.imageViewIcon];
        
        self.labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(62), kHeight(17), kWidth(240),kHeight(15))];
        self.labelTitle.font = [UIFont systemFontOfSize:15];
        self.labelTitle.textColor = UIColorFromRGB(0x333333);
        [self addSubview:self.labelTitle];
        
        UIView *viewLine = [[UIView alloc]initWithFrame:CGRectMake(kWidth(14), kHeight(49), kWidth(292), kHeight(1))];
        viewLine.backgroundColor = UIColorFromRGB(0xf3f3f3);
        [self addSubview:viewLine];
        
        viewLine = [[UIView alloc]initWithFrame:CGRectMake(kWidth(7), 0, kWidth(1), kHeight(50))];
        viewLine.backgroundColor = UIColorFromRGB(0xe5e5e5);
        [self addSubview:viewLine];
        
        viewLine = [[UIView alloc]initWithFrame:CGRectMake(kWidth(312), 0, kWidth(1), kHeight(50))];
        viewLine.backgroundColor = UIColorFromRGB(0xe5e5e5);
        [self addSubview:viewLine];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
