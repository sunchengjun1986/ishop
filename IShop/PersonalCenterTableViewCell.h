

#import <UIKit/UIKit.h>

@interface PersonalCenterTableViewCell : UITableViewCell

@property (retain, nonatomic) UIImageView *imageViewIcon;
@property (retain, nonatomic) UILabel *labelTitle;

@end
