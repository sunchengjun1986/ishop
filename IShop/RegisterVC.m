

#import "RegisterVC.h"
#import "NSString+Expand.h"
#import "SendRequest.h"
@interface RegisterVC ()

@end

static int countDown = 59;

@implementation RegisterVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitle.text = @"Register";
    self.btnBack.hidden = NO;
    self.view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    // Do any additional setup after loading the view.
    UIScrollView *scrollViewMain = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.topToolBarView.frame.size.height,kWidth(320),kScreenHeight-self.topToolBarView.frame.size.height)];
    scrollViewMain.contentSize = CGSizeMake(0, 500);
    scrollViewMain.backgroundColor = UIColorFromRGB(0xf3f3f3);
    [self.view addSubview:scrollViewMain];
    
    //user
    UIView *viewUserNameBG = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), kHeight(64), kWidth(322), kHeight(49))];
    viewUserNameBG.backgroundColor = kWhiteColor;
    viewUserNameBG.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    viewUserNameBG.layer.borderWidth = 1.0;
    [scrollViewMain addSubview:viewUserNameBG];
    
    UILabel *labelUsrName = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(-1), 0, kWidth(105), kHeight(49))];
    labelUsrName.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    labelUsrName.layer.borderWidth = 1.0;
    labelUsrName.text = @"name";
    labelUsrName.textAlignment = NSTextAlignmentCenter;
    labelUsrName.textColor = UIColorFromRGB(0xa6a6a6);
    labelUsrName.backgroundColor = kClearColor;
    labelUsrName.font = [UIFont systemFontOfSize:15];
    [viewUserNameBG addSubview:labelUsrName];

    
    self.textFieldUsrName = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(115), 0, kWidth(200), kHeight(49))];
    self.textFieldUsrName.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldUsrName.font = [UIFont systemFontOfSize:15];
    self.textFieldUsrName.textColor = UIColorFromRGB(0x333333);
    self.textFieldUsrName.delegate = self;
    self.textFieldUsrName.placeholder = @"Enter your USERNAME";
    [viewUserNameBG addSubview:self.textFieldUsrName];
    
    self.viewVerifyCode = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), kHeight(125), kWidth(322), kHeight(49))];
    self.viewVerifyCode.backgroundColor = kWhiteColor;
    self.viewVerifyCode.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    self.viewVerifyCode.layer.borderWidth = 1.0;
    [scrollViewMain addSubview:self.viewVerifyCode];
    
    self.textFieldVerifyCode = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(15), 0, kWidth(160), kHeight(49))];
    self.textFieldVerifyCode.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldVerifyCode.font = [UIFont systemFontOfSize:15];
    self.textFieldVerifyCode.delegate = self;
    self.textFieldVerifyCode.placeholder = @"";
    self.textFieldVerifyCode.textColor = UIColorFromRGB(0x333333);
    [self.viewVerifyCode addSubview:self.textFieldVerifyCode];
    
 
    self.btnVerifyCode = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnVerifyCode setTitle:@"" forState:UIControlStateNormal];
    self.btnVerifyCode.backgroundColor = UIColorFromRGB(0xbbd7e5);
    [self.btnVerifyCode addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.btnVerifyCode.tag = 102;
    [self.btnVerifyCode setTitleColor:kWhiteColor forState:UIControlStateNormal];
    self.btnVerifyCode.titleLabel.font = [UIFont systemFontOfSize:15];
    self.btnVerifyCode.frame = CGRectMake(kWidth(198), kHeight(1), kWidth(124), kHeight(47));
    [self.viewVerifyCode addSubview: self.btnVerifyCode];
    
  self.viewNormal = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), kHeight(189), kWidth(322), kHeight(260))];
    [scrollViewMain addSubview:self.viewNormal];
    
    //password
    UIView *viewPassWd = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), 0, kWidth(322), kHeight(49))];
    viewPassWd.backgroundColor = kWhiteColor;
    viewPassWd.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    viewPassWd.layer.borderWidth = 1.0;
    [self.viewNormal addSubview:viewPassWd];
    
    UILabel *labelPassWd = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(-1), 0, kWidth(105), kHeight(49))];
    labelPassWd.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    labelPassWd.layer.borderWidth = 1.0;
    labelPassWd.text = @"Email";
    labelPassWd.textAlignment = NSTextAlignmentCenter;
    labelPassWd.textColor = UIColorFromRGB(0xa6a6a6);
    labelPassWd.backgroundColor = kClearColor;
    labelPassWd.font = [UIFont systemFontOfSize:15];
    [viewPassWd addSubview:labelPassWd];
    
    
    self.textFieldPassWd = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(115), 0, kWidth(200), kHeight(49))];
    self.textFieldPassWd.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldPassWd.secureTextEntry = YES;
    self.textFieldPassWd.delegate = self;
    self.textFieldPassWd.font = [UIFont systemFontOfSize:15];
    self.textFieldPassWd.placeholder = @"Email address";
    self.textFieldPassWd.textColor = UIColorFromRGB(0x333333);
    [viewPassWd addSubview:self.textFieldPassWd];
    
    //re-enter password
    UIView *viewVerifyPassWd = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), kHeight(62), kWidth(322), kHeight(49))];
    viewVerifyPassWd.backgroundColor = kWhiteColor;
    viewVerifyPassWd.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    viewVerifyPassWd.layer.borderWidth = 1.0;
    [self.viewNormal addSubview:viewVerifyPassWd];
    
    UILabel *labelVerifyPassWd = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(-1), 0, kWidth(105), kHeight(49))];
    labelVerifyPassWd.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    labelVerifyPassWd.layer.borderWidth = 1.0;
    labelVerifyPassWd.text = @"Password";
    labelVerifyPassWd.textAlignment = NSTextAlignmentCenter;
    labelVerifyPassWd.textColor = UIColorFromRGB(0xa6a6a6);
    labelVerifyPassWd.backgroundColor = kClearColor;
    labelVerifyPassWd.font = [UIFont systemFontOfSize:15];
    [viewVerifyPassWd addSubview:labelVerifyPassWd];
    
    self.textFieldVerifyPassWd = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(115), 0, kWidth(200), kHeight(49))];
    self.textFieldVerifyPassWd.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldVerifyPassWd.font = [UIFont systemFontOfSize:15];
    self.textFieldVerifyPassWd.secureTextEntry = YES;
    self.textFieldVerifyPassWd.delegate = self;
    self.textFieldVerifyPassWd.placeholder = @"Password";
    self.textFieldVerifyPassWd.textColor = UIColorFromRGB(0x333333);
    [viewVerifyPassWd addSubview:self.textFieldVerifyPassWd];
    
  
    
    //register button
    UIButton *btnRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRegister setTitle:@"Register" forState:UIControlStateNormal];
    btnRegister.tag = 101;
    [btnRegister addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    btnRegister.backgroundColor = kBackGrColor;
    btnRegister.layer.cornerRadius = 5.0;
    [btnRegister setTitleColor:kWhiteColor forState:UIControlStateNormal];
    btnRegister.titleLabel.font = [UIFont systemFontOfSize:15];
    btnRegister.frame = CGRectMake(kWidth(15), kHeight(215), kWidth(290), kHeight(44));
    [self.viewNormal addSubview: btnRegister];
    
    self.viewNormal.frame = CGRectMake(-1, kHeight(125), kWidth(322), kHeight(321));//如果要将验证码隐藏的坐标
    self.strCodeExist = @"0";
}
-(void)viewDidAppear:(BOOL)animated{

}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    }];
    [self.view endEditing:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    }];
    [self.view endEditing:NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.textFieldPassWd||textField == self.textFieldVerifyPassWd|| textField == self.textFieldInvitedCode )
    {
        [UIView animateWithDuration:0.25 animations:^{
            self.view.frame = CGRectMake(0, -160, kScreenWidth, kScreenHeight);
        }];
    }else
    {
        [UIView animateWithDuration:0.25 animations:^{
            self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        }];
    }
}


- (void)buttonOnClick:(UIButton *)btn
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    }];
    [self.view endEditing:YES];
    if (btn.tag == 101) { // register
        if ( self.textFieldUsrName.text.length == 0)
        {
            UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter your USERNAME" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
        }else if (self.textFieldPassWd.text.length == 0)
        {
            UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter your email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
        }
        else if (self.textFieldVerifyPassWd.text.length == 0)
        {
            UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter your password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
        }
        else{
            [self showProgressWithView:self.view animated:YES];
            
            [SendRequest userWithUserName:self.textFieldUsrName.text result:^(NSDictionary *result, NSError *error) {
                [self hideProgress:self.view animated:YES];
                if ([[result objectForKey:@"errorCode"] isEqualToString:@"1"]) {
                    [[CommonUtils sharedCommonUtils]showAlert:@"Account already exists" delegate:nil];
                }else{
                    [SendRequest registerWithUserName:self.textFieldUsrName.text passWord:self.textFieldPassWd.text type:self.textFieldVerifyPassWd.text result:^(NSDictionary *result, NSError *error) {
                        [self hideProgress:self.view animated:YES];
                        if ([[result objectForKey:@"errorCode"] isEqualToString:@"1"]) {
                            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                            
                            [defaults setObject:self.textFieldUsrName.text forKey:@"userName"];
                            [defaults setObject:self.textFieldVerifyPassWd.text forKey:@"email"];
                            [defaults synchronize];
                            [[CommonUtils sharedCommonUtils]showAlert:@"The success of the registration" delegate:self];
                        }else{
                            [[CommonUtils sharedCommonUtils]showAlert:@"Registration failed" delegate:nil];
                        }
                        
                    }];
                }
                
            }];

            
           
        }
    }else if (btn.tag == 102){
       
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    if (buttonIndex == 0) {
    }
    
}

- (void)countTime
{
    NSString *strCountDown = [NSString stringWithFormat:@"%d",countDown--];
    [self.btnVerifyCode setTitle:strCountDown forState:UIControlStateNormal];
    if (countDown == -1) {
        countDown = 59;
        [_t invalidate];
        self.btnVerifyCode.userInteractionEnabled = YES;
        self.btnVerifyCode.backgroundColor = UIColorFromRGB(0xbbd7e5);
        [self.btnVerifyCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    }
}

-(BOOL) isValidateMobile:(NSString *)mobile
{
 
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(14[0,0-9])|(18[0,0-9])|(17[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

- (void)btnBackClicked:(id)sender
{
    [_t invalidate];
    [self.navigationController popViewControllerAnimated:YES];
    countDown = 59;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  }



@end
