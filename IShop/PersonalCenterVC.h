
#import "BaseViewController.h"

@interface PersonalCenterVC : BaseViewController <UIAlertViewDelegate>

@property (retain, nonatomic) NSMutableArray *arrName;
@property (retain, nonatomic) UITableView *tabelView;
@property (retain, nonatomic) UIView *viewBg;
@property (retain, nonatomic) UILabel *label1Line2;
@property (retain, nonatomic) UILabel *labelKind;
@property (retain, nonatomic) UILabel *labelYuENum;
@property (retain, nonatomic) UILabel *labelJiFenNum;
@property (retain, nonatomic) UILabel *labelPhone;
@property (retain, nonatomic) UILabel *labelLocation;
@end
