

#import <UIKit/UIKit.h>
#import "DPAPI.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    BOOL _isFirst;
}
@property (strong, nonatomic) UIWindow *window;
@property (readonly, nonatomic) DPAPI *dpapi;
@property (strong, nonatomic) NSString *appKey;
@property (strong, nonatomic) NSString *appSecret;
+ (AppDelegate *)instance;
@end
