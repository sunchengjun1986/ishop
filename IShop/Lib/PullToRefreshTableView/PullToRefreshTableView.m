

#import "PullToRefreshTableView.h"
#import <QuartzCore/QuartzCore.h>



@implementation StateView

@synthesize indicatorView;
@synthesize arrowView;
@synthesize stateLabel;
@synthesize timeLabel;
@synthesize viewType;
@synthesize currentState;

- (id)initWithFrame:(CGRect)frame viewType:(int)type
{
    CGFloat width = frame.size.width;
    
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, width, k_STATE_VIEW_HEIGHT)];
    
    if (self) {
      
        viewType = type == k_VIEW_TYPE_HEADER ? k_VIEW_TYPE_HEADER : k_VIEW_TYPE_FOOTER;
        self.backgroundColor = [UIColor clearColor];
        
     
        UIActivityIndicatorView *indicatorViewTemp = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((k_STATE_VIEW_INDICATE_WIDTH - 20) / 2, (k_STATE_VIEW_HEIGHT - 20) / 2, 20, 20)];
        self.indicatorView = indicatorViewTemp;
        self.indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.indicatorView.hidesWhenStopped = YES;
        [self addSubview:self.indicatorView];
        
  
        UIImageView *arrowViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake((k_STATE_VIEW_INDICATE_WIDTH - 32) / 2, (k_STATE_VIEW_HEIGHT - 32) / 2, 32, 32)];
        NSString * imageNamed = type == k_VIEW_TYPE_HEADER ? @"arrow_down" : @"arrow_up";
        self.arrowView = arrowViewTemp;
        self.arrowView.image = [UIImage imageNamed:imageNamed];
        [self addSubview:self.arrowView];
        
   
        UILabel *stateLabelTemp = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
        self.stateLabel = stateLabelTemp;
        self.stateLabel.font = [UIFont systemFontOfSize:12.0f];
        self.stateLabel.backgroundColor = [UIColor clearColor];
        self.stateLabel.textAlignment = NSTextAlignmentCenter;
        self.stateLabel.text = type == k_VIEW_TYPE_HEADER ? @"pull down" : @"pull up";
        self.stateLabel.textColor = [UIColor lightGrayColor];
        [self addSubview:self.stateLabel];
        
  
        UILabel *timeLabelTemp = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, width, k_STATE_VIEW_HEIGHT - 20 )];
        self.timeLabel = timeLabelTemp;
        self.timeLabel.font = [UIFont systemFontOfSize:12.0f];
        self.timeLabel.backgroundColor = [UIColor clearColor];
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        NSDate * date = [NSDate date];
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterFullStyle];
        [formatter setDateFormat:@"MM-dd HH:mm"];
        self.timeLabel.text = [NSString stringWithFormat:@"last time %@", [formatter stringFromDate:date]];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        [self addSubview:self.timeLabel];
        
    }
    return self;
}

- (void)changeState:(int)state{
    [indicatorView stopAnimating];
    arrowView.hidden = NO;
    [UIView beginAnimations:nil context:nil];
    switch (state) {
        case k_PULL_STATE_NORMAL:
            currentState = k_PULL_STATE_NORMAL;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"pull down to refresh" : @"pull up to load more";
            //  arrowView
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            break;
        case k_PULL_STATE_DOWN:
            currentState = k_PULL_STATE_DOWN;
            stateLabel.text = @"release to refresh";
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            break;
            
        case k_PULL_STATE_UP:
            currentState = k_PULL_STATE_UP;
            stateLabel.text = @"release to load data";
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            break;
            
        case k_PULL_STATE_LOAD:
            currentState = k_PULL_STATE_LOAD;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"refresh.." : @"loding..";
            [indicatorView startAnimating];
            arrowView.hidden = YES;
            break;
            
        case k_PULL_STATE_END:
            currentState = k_PULL_STATE_END;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? stateLabel.text : @"loded all data";
            arrowView.hidden = YES;
            break;
            
        default:
            currentState = k_PULL_STATE_NORMAL;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"pull down to refresh" : @"pull up to load more";
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            break;
    }
    [UIView commitAnimations];
}

- (void)updateTimeLabel{
    NSDate * date = [NSDate date];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterFullStyle];
    [formatter setDateFormat:@"MM-dd HH:mm"];
     timeLabel.text = [NSString stringWithFormat:@"lastime refresh %@", [formatter stringFromDate:date]];
}


@end





@implementation PullToRefreshTableView

@synthesize headerView;
@synthesize footerView;

@synthesize delegate_PullDown = _delegate_PullDown;
@synthesize delegate_PullUp = _delegate_PullUp;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _times = 0;
        headerView = [[StateView alloc] initWithFrame:CGRectMake(0, -40, frame.size.width, frame.size.height) viewType:k_VIEW_TYPE_HEADER];
        footerView = [[StateView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) viewType:k_VIEW_TYPE_FOOTER];
        [self addSubview:headerView];
        [self setTableFooterView:footerView];
    }
    return self;
}
- (void)loadDataWithFrame:(CGRect)frame{
    _times = 0;
    headerView = [[StateView alloc] initWithFrame:CGRectMake(0, -40, kScreenWidth, frame.size.height) viewType:k_VIEW_TYPE_HEADER];
    footerView = [[StateView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, frame.size.height) viewType:k_VIEW_TYPE_FOOTER];
    [self addSubview:headerView];
    [self setTableFooterView:footerView];
}
- (void)tableViewDidDragging{
    CGFloat offsetY = self.contentOffset.y;
    //  boolean
    if (headerView.currentState == k_PULL_STATE_LOAD ||
        footerView.currentState == k_PULL_STATE_LOAD) {
        return;
    }
    //
    if (offsetY < -k_STATE_VIEW_HEIGHT - 10) {
        [headerView changeState:k_PULL_STATE_DOWN];
    } else {
        [headerView changeState:k_PULL_STATE_NORMAL];
    }
    //
    if (footerView.currentState == k_PULL_STATE_END) {
        return;
    }
    //
    CGFloat differenceY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) : 0;
    //
    if (offsetY > differenceY + k_STATE_VIEW_HEIGHT / 5 * 1) {
       
        [footerView changeState:k_PULL_STATE_LOAD];
        
        [self.delegate_PullUp pullUpEvent];
        
        
        //        return k_RETURN_LOADMORE;
    } else {
        [footerView changeState:k_PULL_STATE_NORMAL];
    }
}

- (NSInteger)tableViewDidEndDragging{
    CGFloat offsetY = self.contentOffset.y;
    //
    if (headerView.currentState == k_PULL_STATE_LOAD ||
        footerView.currentState == k_PULL_STATE_LOAD) {
        return k_RETURN_DO_NOTHING;
    }
    //
    if (offsetY < -k_STATE_VIEW_HEIGHT - 10) {
        [headerView changeState:k_PULL_STATE_LOAD];
        self.contentInset = UIEdgeInsetsMake(-offsetY, 0, 0, 0);
        
        [self.delegate_PullDown pullDownEvent];
        
        return k_RETURN_REFRESH;
    }
    //
    CGFloat differenceY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) : 0;
    if (footerView.currentState != k_PULL_STATE_END &&
        offsetY > differenceY + k_STATE_VIEW_HEIGHT / 3 * 2)
       
    {
        
        if(footerView.currentState != k_PULL_STATE_NORMAL)
        {
            [footerView changeState:k_PULL_STATE_LOAD];
        }
        
        return k_RETURN_LOADMORE;
    }
    return k_RETURN_DO_NOTHING;
}

- (void)reloadData:(BOOL)dataIsAllLoaded{
    [self reloadData];
    self.contentInset = UIEdgeInsetsZero;
    [headerView changeState:k_PULL_STATE_NORMAL];
   
    if (dataIsAllLoaded) {
        [footerView changeState:k_PULL_STATE_END];
           } else {
        [footerView changeState:k_PULL_STATE_NORMAL];
     
    }
  
    [headerView updateTimeLabel];
    [footerView updateTimeLabel];
}

@end