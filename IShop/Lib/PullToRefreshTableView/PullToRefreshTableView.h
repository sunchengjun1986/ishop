
#import <UIKit/UIKit.h>

#define k_PULL_STATE_NORMAL         0     
#define k_PULL_STATE_DOWN           1
#define k_PULL_STATE_LOAD           2
#define k_PULL_STATE_UP             3
#define k_PULL_STATE_END            4

#define k_RETURN_DO_NOTHING         0
#define k_RETURN_REFRESH            1
#define k_RETURN_LOADMORE           2

#define k_VIEW_TYPE_HEADER          0
#define k_VIEW_TYPE_FOOTER          1

#define k_STATE_VIEW_HEIGHT         40
#define k_STATE_VIEW_INDICATE_WIDTH 60



@protocol TablePullDownEvent <NSObject>

@required

- (void)pullDownEvent;

@end

@protocol TablePullUpEvent <NSObject>

@required

- (void)pullUpEvent;

@end



@interface StateView : UIView {
@private
    UIActivityIndicatorView * indicatorView;
    UIImageView             * arrowView;
    UILabel                 * stateLabel;
    UILabel                 * timeLabel;
    int                       viewType;
    int                       currentState;
}

@property (nonatomic, retain) UIActivityIndicatorView * indicatorView;
@property (nonatomic, retain) UIImageView             * arrowView;
@property (nonatomic, retain) UILabel                 * stateLabel;
@property (nonatomic, retain) UILabel                 * timeLabel;
@property (nonatomic)         int                       viewType;
@property (nonatomic)         int                       currentState;


- (id)initWithFrame:(CGRect)frame viewType:(int)type;


- (void)changeState:(int)state;


- (void)updateTimeLabel;

@end



@interface PullToRefreshTableView : UITableView{
    StateView * headerView;
    StateView * footerView;
    
    NSInteger _times;
}

@property (nonatomic, retain) StateView * headerView;
@property (nonatomic, retain) StateView * footerView;

@property (nonatomic, assign) id<TablePullUpEvent> delegate_PullUp;
@property (nonatomic, assign) id<TablePullDownEvent> delegate_PullDown;
- (void)loadDataWithFrame:(CGRect)frame;

- (void)tableViewDidDragging;


- (NSInteger)tableViewDidEndDragging;


- (void)reloadData:(BOOL)dataIsAllLoaded;

@end