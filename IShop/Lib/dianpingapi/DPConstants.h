

#ifndef apidemo_DPConstants_h
#define apidemo_DPConstants_h

#define DPAPIVersion                @"2.0"

#define kDPAPIErrorDomain           @"DPAPIErrorDomain"
#define kDPAPIErrorCodeKey          @"DPAPIErrorCodeKey"

#define kDPAPIDomain				@"http://api.dianping.com/"

#endif
