

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, YLSwipeLockNodeViewStatus) {
    YLSwipeLockNodeViewStatusNormal,
    YLSwipeLockNodeViewStatusSelected,
    YLSwipeLockNodeViewStatusWarning
};

@interface YLSwipeLockNodeView : UIView
@property (nonatomic) YLSwipeLockNodeViewStatus nodeViewStatus;

@end
