



#import "SDDNetWorkUtils.h"
#import <QuartzCore/QuartzCore.h>

@implementation SDDNetWorkUtils




+ (SDDNetWorkStatus)currentNetworkStatus {
    NetworkStatus wifiStatusTemp = [[Reachability reachabilityForLocalWiFi] 
                                    currentReachabilityStatus];
    if (wifiStatusTemp == ReachableViaWiFi) {
        return SDDNewWorkStatusWIFI;
    };
    NetworkStatus connStatusTemp = [[Reachability reachabilityForInternetConnection] 
                                    currentReachabilityStatus];
    if (connStatusTemp == NotReachable) {
        return SDDNewWorkStatusNone;
    }
    return SDDNewWorkStatusWWAN;
}


+ (BOOL)isHostReachable{
    NetworkStatus connStatusTemp = [[Reachability reachabilityForInternetConnection]
                                    currentReachabilityStatus];
    if (connStatusTemp == NotReachable) {
        return NO;
    }else{
        return YES;
    }
}

@end
