

#ifndef DiscoveryLand_Common_h
#define DiscoveryLand_Common_h
#define NavigationBar_HEIGHT 44
#define UILABEL_DEFAULT_FONT_SIZE 20.0f

/********System*******/
//system detection
#define CurrentSystemVersion ([[UIDevice currentDevice] systemVersion])

#define IOS7Later [[[UIDevice currentDevice] systemVersion]floatValue]>=7

//device detection
#define iPhone4 ([UIScreen mainScreen].bounds.size.height == 480)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size)) : NO)
#define iPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kWidth(R) (R)*(kScreenWidth)/320
#define kHeight(R) (iPhone4?((R)*(kScreenHeight)/480):((R)*(kScreenHeight)/568))
/********colour***********/

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define kColor(R,G,B,A) [UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]
#define kWhiteColor     [UIColor whiteColor]
#define kLightGrayColor [UIColor lightGrayColor]
#define kBlackColor     [UIColor blackColor]
#define kClearColor     [UIColor clearColor]
#define kGrayColor      [UIColor grayColor]
#define kRedColor      [UIColor redColor]
#define kYellowColor      [UIColor yellowColor]

#define kGreenColor      [UIColor greenColor]
#define kLineColor UIColorFromRGB(0xdbdbdb)

#define kBackGrColor UIColorFromRGB(0xDA4D39)
//system log
#if 1
#define SaintiLog(x, ...) NSLog(x, ## __VA_ARGS__);
#else
#define SaintiLog(x, ...)
#endif
//#define BASEURL @"http://demo.asiasea.com.cn/app/"//post request test
#define BASEURL @"http://www.asae.cn/app/"//post request

//APPID，replace when applition initial
#define APPID @"863892746"

#define SAFE_RELEASE(obj)     [obj release]; obj = nil;

//#define IMAGE(img) iPhone6?[UIImage imageNamed:[NSString stringWithFormat:@"%@6-",img]]:[UIImage imageNamed:img]
#define IMAGE1(img) (iPhone6?[UIImage imageNamed:[NSString stringWithFormat:@"%@-6",img]]:[UIImage imageNamed:img])
#define IMAGE(img) [UIImage imageNamed:img]
#define GetImageByName(name) iPhone6?[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@-6",name] ofType:@"png"]]:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@"png"]]
#endif

//Nslog
#ifdef SHOW_S_LOG
#define SLogDetail(s, ... ) NSLog(@"\r\nFile: ---------------------> %s\r\n---------------------> %s :Line->%d \r\nlogInfo=> %@",__FILE__,__func__, __LINE__,[NSString stringWithFormat:(s), ##__VA_ARGS__]);
#define SLog(s, ... ) NSLog(@"\r\n\r\n------------------------logInfo start------------------------\n%@",[NSString stringWithFormat:(s), ##__VA_ARGS__]);
#elif SHOW_S_LOG_AND_CONSOLE
#define SLogDetail(s, ... ) NSLog(@"\r\nFile: ---------------------> %s\r\n---------------------> %s :Line->%d \r\nlogInfo=> %@",__FILE__,__func__, __LINE__,[NSString stringWithFormat:(s), ##__VA_ARGS__]);[iConsole info:@"File: ----> %s\r\n------> %s :Line->%d \r\n---> %@",__FILE__,__func__, __LINE__,[NSString stringWithFormat:(s), ##__VA_ARGS__]];
#define SLog(s, ... ) NSLog(@"\r\n---------------------logInfo---------------------\n%@",[NSString stringWithFormat:(s), ##__VA_ARGS__]); [iConsole info:@"---------------------logInfo---------------------\n%@",[NSString stringWithFormat:(s), ##__VA_ARGS__]];
#elif SHOW_S_LOG_NONE
#define SLogDetail(s, ... )
#define SLog(s, ... )
#endif

//printf
#ifdef SHOW_S_PRINTF
#define SPrintf(s, ... ) (printf("\r\n---> SPrintf =>" s,##__VA_ARGS__));
#else
#define SPrintf(s, ... )
#endif
/********Internet request*******/
//request fail
#define ERRORCODE @"errorCode"
#define MESSAGE @"message"
#define kHTTPRequestFormater @"{\"errCode\":\"%@\",\"rspMsg\":\"%@\"}"

//successul connection
#define kSuccessResultCode @"0"

//Can not connect internet
#define kNoNetworkResultCode @"9001"

#define kNoNetworkResultMsg @"test your network"

//server reply blank
#define kBadResDataResultCode @"9002"

//server reply blank
#define kEmptyResultCode @"9003"

//server reply blank JSON
#define kEmptyResultMsg @"fail，retry"

//request size by page
#define kNumOfPageSize @"10"
#define kNumOfPageIntSize 10
#define kPagePerModelOutTime [PagePerModel sharedInstance].strOutTime
#define kPagePerModelPerPageFollow [PagePerModel sharedInstance].strPerPageFollow
#define kPagePerModelPerPageMyOrd [PagePerModel sharedInstance].strPerPageMyOrd
#define kPagePerModelPerPageOften [PagePerModel sharedInstance].strPerPageOften
#define kPagePerModelPerPagePro [PagePerModel sharedInstance].strPerPagePro

#define kStartPage 0
#define k12Font [UIFont systemFontOfSize:12.0f]
#define k14Font [UIFont systemFontOfSize:14.0f]
#define k15Font [UIFont systemFontOfSize:15.0f]
#define k18Font [UIFont systemFontOfSize:18.0f]

//UserDefault
#define USERDEFAULTS NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults]

#define KUserInfoModel UserInfoModel *userModel = [UserInfoModel sharedInstance];

#define KPagePerModel PagePerModel *pagePerModel = [PagePerModel sharedInstance];