

#import <Foundation/Foundation.h>
#import "SDDNetWorkUtils.h"
#import "MBProgressHUD.h"
#import <SystemConfiguration/SystemConfiguration.h>
@interface CommonUtils : NSObject
@property(nonatomic,retain) UIAlertView *alertView;

+ (instancetype)sharedCommonUtils;

-(void) showAlert:(NSString *) oStrMsg
         delegate:(id<UIAlertViewDelegate>) delegate;

+ (NSString *)devicesVersion;
+ (UILabel*)createLabelWithText:(NSString*)string frame:(CGRect)frame;

+ (UITextView*)createTextViewWithText:(NSString*)string frame:(CGRect)frame;

+ (UITextField*)createTextFieldWithText:(NSString*)string frame:(CGRect)frame;

+ (UIButton*)createButtonWithText:(NSString*)string frame:(CGRect)frame;

+ (void)showTipOnView:(UIView *)aView
           withTarget:(id)target
          andSelector:(SEL)action
             andTitle:(NSString *)title;

+ (void)hideTipOnView:(UIView *)aView;

+ (NSString *)communityTimeWithDate:(NSDate *)date;

+ (NSString *)messageTimeWithDate:(NSDate *)date;

+ (void)showToast:(NSString *)strTest;

+(BOOL) isNetworkEnabled;
@end
