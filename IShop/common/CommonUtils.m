

#import "CommonUtils.h"
#define kdefaultFont [UIFont systemFontOfSize:15.0f]
#define kNoNetImgViewKey   2917
#define kBtnTag            3918
#define kNoDataTitleColor      kColor(160,160,160,1)
#define kBackGrayColor         kColor(221,221,221,1)
#define kNoDataTitleSize 18
@implementation CommonUtils
+ (instancetype)sharedCommonUtils {
    static CommonUtils *_commonUtils = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _commonUtils = [[CommonUtils alloc] init];
    });
    
    return _commonUtils;
}

-(void) showAlert:(NSString *) oStrMsg
         delegate:(id<UIAlertViewDelegate>) delegate {
    [self.alertView dismissWithClickedButtonIndex:0 animated:NO];
    self.alertView =[[UIAlertView alloc] initWithTitle:@"Warning:"
                                               message:oStrMsg
                                              delegate:delegate
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil ];
    self.alertView.tag=888;
    [self.alertView show];
}

+ (NSString *)devicesVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}
//
//static const CGFloat borderWidth = 0.5;

+ (UILabel*)createLabelWithText:(NSString*)string frame:(CGRect)frame {
    UILabel *result = [[UILabel alloc] initWithFrame:frame];
    result.numberOfLines = 0;
    result.backgroundColor = [UIColor clearColor];
    result.textColor = [UIColor whiteColor];
    result.text = string;
    result.font = kdefaultFont;
//    result.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    result.layer.borderWidth = borderWidth;
    
    return result ;
}

+ (UITextView*)createTextViewWithText:(NSString*)string frame:(CGRect)frame {
    UITextView *result = [[UITextView alloc] initWithFrame:frame];
    result.text = string;
    result.backgroundColor = [UIColor clearColor];
    result.textColor = [UIColor whiteColor];
    result.font = kdefaultFont;
    
    return result;
}

+ (UITextField*)createTextFieldWithText:(NSString*)string frame:(CGRect)frame {
    UITextField *result = [[UITextField alloc] initWithFrame:frame];
    result.text = string;
    result.backgroundColor = [UIColor clearColor];
    result.textColor = [UIColor whiteColor];
    result.font = kdefaultFont;
    
    return result;
}

+ (UIButton*)createButtonWithText:(NSString*)string frame:(CGRect)frame {
    UIButton *result = [UIButton buttonWithType:UIButtonTypeCustom];
    [result setFrame:frame];
    [result setTitle: string forState:UIControlStateNormal];
    result.backgroundColor = [UIColor clearColor];
    [result setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return result;
}

+ (void)showTipOnView:(UIView *)aView
           withTarget:(id)target
          andSelector:(SEL)action
             andTitle:(NSString *)title
{
    
    BOOL isFindoutShade = NO;
    
    for (UIView *tempView in aView.subviews )
    {
        if (tempView.tag == kNoNetImgViewKey)
        {
            tempView.hidden = NO;
            isFindoutShade = YES;
            [aView bringSubviewToFront:tempView];
            
            for (UIView *view in tempView.subviews) {
                if (view.tag==kBtnTag) {
                    UIButton *tempBtn=(UIButton *)view;
                    [tempBtn setTitle:title forState:UIControlStateNormal];
                }
                
            }
        }
        
        
        
    }
    
    if (!isFindoutShade)
    {
        float viewWidth  = aView.frame.size.width;
        float viewHeight = aView.frame.size.height;
        //            [aView setBackgroundColor:[UIColor redColor]];
        CGRect shadeImgViewRect = CGRectMake(0, 64, viewWidth, viewHeight - 64);
        UIImageView *shadeView = [[UIImageView alloc] initWithFrame:shadeImgViewRect];
        [shadeView setUserInteractionEnabled:YES];
        shadeView.tag = kNoNetImgViewKey;
        shadeView.backgroundColor = kWhiteColor;
        
        UIButton *noDataBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [noDataBtn setFrame:CGRectMake(0, 0, aView.frame.size.width, aView.frame.size.height)];
        //            [[UIButton alloc]initWithFrame:labRect];
        noDataBtn.tag = kBtnTag;
        noDataBtn.backgroundColor = [UIColor clearColor];
        [noDataBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:kNoDataTitleSize]];
        [noDataBtn setTitleColor:kNoDataTitleColor forState:UIControlStateNormal];
        [noDataBtn.titleLabel setTextColor:kNoDataTitleColor];
        [noDataBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        //        [noDataBtn setBackgroundColor:[UIColor clearColor]];
        [noDataBtn setTitle:title forState:UIControlStateNormal];
        [noDataBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        [shadeView addSubview:noDataBtn];

        UIImageView *noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 35)];
        noDataView.image = IMAGE(@"noData");
        noDataView.center =  shadeView.center;
        [shadeView addSubview:noDataView];
      
     
        [aView addSubview:shadeView];
    }
    
}

+ (void)hideTipOnView:(UIView *)aView
{
    for (UIView *tempView in aView.subviews )
    {
        if (tempView.tag == kNoNetImgViewKey)
        {
            tempView.hidden = YES;
        }
        
    }
    
}

+ (NSString *)communityTimeWithDate:(NSDate *)date{
    NSTimeInterval late=[date timeIntervalSince1970]*1;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    
     NSTimeInterval cha=now-late;
    NSString *timeString=@"";
    if (cha/3600<=1) {
        timeString = [NSString stringWithFormat:@"%f", cha/60];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@分钟前", timeString];
        
    }
    if (cha/3600>1&&cha/86400<=1) {
        timeString = [NSString stringWithFormat:@"%f", cha/3600];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@小时前", timeString];
    }
    
    if (cha/86400>1&&cha/604800<=1)
    {
        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@天前", timeString];

    }
    if (cha/604800>1)
    {

        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"YYYY-MM-dd"];
        timeString = [NSString stringWithFormat:@"%@",[dateformatter stringFromDate:date]];
    }
    return timeString;
}

+ (NSString *)messageTimeWithDate:(NSDate *)date{
    NSTimeInterval late=[date timeIntervalSince1970]*1;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    
    NSTimeInterval cha=now-late;
    NSString *timeString=@"";
    if (cha/86400<=1) {

        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"HH:mm"];
        timeString = [NSString stringWithFormat:@"%@",[dateformatter stringFromDate:date]];

        
    }
    if (cha/86400>1&&cha/172800<=1) {
        timeString = @"yesterday";

    }
    
    if (cha/172800>1)
    {
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"MM-dd"];
        timeString = [NSString stringWithFormat:@"%@",[dateformatter stringFromDate:date]];    }
       return timeString;

}

+ (void)showToast:(NSString *)strTest{
    MBProgressHUD *loading = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    loading.mode = MBProgressHUDModeText;
    loading.color = [UIColor lightGrayColor];
    loading.labelText = strTest;
    
    [self performSelector:@selector(delayMethod:) withObject:nil afterDelay:1.5f];
    
}
+ (void)delayMethod:(id)obj{
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
}

+(BOOL) isNetworkEnabled
{
    BOOL bEnabled = FALSE;
    NSString *url = @"www.baidu.com";
    SCNetworkReachabilityRef ref = SCNetworkReachabilityCreateWithName(NULL, [url UTF8String]);
    SCNetworkReachabilityFlags flags;
    
    bEnabled = SCNetworkReachabilityGetFlags(ref, &flags);
    
    CFRelease(ref);
    if (bEnabled) {
 
        BOOL flagsReachable = ((flags & kSCNetworkFlagsReachable) != 0);
        BOOL connectionRequired = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
        BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
        bEnabled = ((flagsReachable && !connectionRequired) || nonWiFi) ? YES : NO;
    }
    
    return bEnabled;
}
@end
