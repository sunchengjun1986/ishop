

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "Reachability.h"

typedef enum {
    SDDNewWorkStatusNone,
    SDDNewWorkStatusWIFI,
    SDDNewWorkStatusWWAN
} SDDNetWorkStatus;


@interface SDDNetWorkUtils : NSObject<UIAlertViewDelegate>

+ (SDDNetWorkStatus) currentNetworkStatus;


+ (BOOL)isHostReachable;



@end
