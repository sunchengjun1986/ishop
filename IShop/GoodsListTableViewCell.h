
#import <UIKit/UIKit.h>

@interface GoodsListTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (retain, nonatomic) UIImageView *imageViewIcon;
@property (retain, nonatomic) UILabel *labelName;
@property (retain, nonatomic) UILabel *labelPriceYuan;
@property (retain, nonatomic) UILabel *labelPriceCent;
@property (retain, nonatomic) UITextField *textFiledNumber;
@property (retain, nonatomic) UIButton *btnDanwei;
@property (retain, nonatomic) UILabel *labelDanwei;
@property (retain, nonatomic) UIButton *btnAddToCart;
@property (copy , nonatomic)  NSString *strSerialNumber;

@end
