

#import "BaseViewController.h"
#import "AppDelegate.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pic_背景.png"]];

//	 Do any additional setup after loading the view.
    UIView *viewStatusBarBk = [[UIView alloc]initWithFrame:CGRectMake(0, -20, 320, 20)];
    viewStatusBarBk.backgroundColor = UIColorFromRGB(0xDA4D39);
    [self.view addSubview:viewStatusBarBk];
    [self addTitleView];
}

- (void)paningGestureReceive:(UISwipeGestureRecognizer*)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];

    
}

- (void)addTitleView
{
    self.topToolBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NavigationBar_HEIGHT)];;
    //导航条颜色
    self.topToolBarView.backgroundColor = UIColorFromRGB(0xDA4D39);
    
    [self.view addSubview:self.topToolBarView];
    
    [self.view addSubview:self.topToolBarView];
    
    //增加back 按钮
    self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"按钮_返回_正常.png"] forState:UIControlStateNormal];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"按钮_返回_点击.png"] forState:UIControlStateHighlighted];
    [self.btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topToolBarView addSubview:self.btnBack];

    [self.btnBack setFrame:CGRectMake(5, 0.5 , 44, 44)];//yubo

    
    self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20,0,kScreenWidth-40,NavigationBar_HEIGHT)];
    self.lblTitle.text = self.title;
    
 
    self.lblTitle.backgroundColor = [UIColor clearColor];
    self.lblTitle.textAlignment = NSTextAlignmentCenter;
    self.lblTitle.textColor = [UIColor whiteColor];
    self.lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:UILABEL_DEFAULT_FONT_SIZE];
    [self.topToolBarView addSubview:self.lblTitle];
}

- (void) viewDidLayoutSubviews {
    if ([[[UIDevice currentDevice] systemVersion]floatValue]>= 7) {
        CGRect viewBounds = self.view.bounds;
        CGFloat topBarOffset = self.topLayoutGuide.length;
        viewBounds.origin.y = topBarOffset * -1;
        self.view.bounds = viewBounds;
    }
}



- (void)showProgressWithView:(UIView*)view animated:(BOOL)isAnimated{
    [self hideProgress:view animated:NO];
    
    MBProgressHUD *loading = [MBProgressHUD showHUDAddedTo:view animated:isAnimated];
    loading.mode=MBProgressHUDModeIndeterminate;
    loading.color = [UIColor lightGrayColor];
    
    _hideProgressTimer = [NSTimer scheduledTimerWithTimeInterval:20.0f
                                                          target:self
                                                        selector:@selector(handleTimer:)
                                                        userInfo:view
                                                         repeats:NO];
}

- (void)hideProgress:(UIView*)view animated:(BOOL)isAnimated{
    
    if (nil != _hideProgressTimer && [_hideProgressTimer isValid]) {
        [_hideProgressTimer invalidate];
             _hideProgressTimer = nil;
    }
    [MBProgressHUD hideHUDForView:view animated:isAnimated];
    
}


- (void)handleTimer:(NSTimer *)timer {
    
    [self hideProgress:[timer userInfo] animated:YES];
}
- (void)btnBackClicked:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

@end
