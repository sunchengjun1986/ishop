

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject
@property(nonatomic,strong) NSString *errorCode;
@property(nonatomic,strong) NSString *errMsg;
@property(nonatomic,strong) NSMutableArray  *arrData;
-(id)initWithDic:(NSDictionary *)data;
@end
