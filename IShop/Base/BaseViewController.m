/*
 ***************************************************************************
 * 建立日期	： 2014-04-15
 * 版权声明	： 本代码版权归圣笛科技所有，禁止任何未授权的传播和使用
 * 作者		： yanjiaming@sainti.com
 * 模块		：
 * 描述		： BaseViewController
 * -------------------------------------------------------------------------
 * 修改历史
 * 序号			日期					修改人				修改原因
 * <#序号#>        <#日期#>                <#修改人#>              <#修改原因#>
 *
 ***************************************************************************
 */

#import "BaseViewController.h"
#import "AppDelegate.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pic_背景.png"]];

	// Do any additional setup after loading the view.
    UIView *viewStatusBarBk = [[UIView alloc]initWithFrame:CGRectMake(0, -20, 320, 20)];
    viewStatusBarBk.backgroundColor = [UIColor blackColor];
    [self.view addSubview:viewStatusBarBk];
    [self addTitleView];
}

- (void)paningGestureReceive:(UISwipeGestureRecognizer*)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];

    
}

- (void)addTitleView
{
    self.topToolBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_HEIGHT)];;
    //导航条颜色
    self.topToolBarView.backgroundColor = UIColorFromRGB(0xDA4D39);
    
    [self.view addSubview:self.topToolBarView];
    
    self.topToolBarView.backgroundColor = [UIColor colorWithPatternImage:[[UIImage imageNamed:@"导航栏.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:0]];
    [self.view addSubview:self.topToolBarView];
    
    //增加back 按钮
    self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"按钮_返回_正常.png"] forState:UIControlStateNormal];
    [self.btnBack setBackgroundImage:[UIImage imageNamed:@"按钮_返回_点击.png"] forState:UIControlStateHighlighted];
    [self.btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.topToolBarView addSubview:self.btnBack];
    
    //    if(self.sPageNavController.rootViewController != self){
    [self.btnBack setFrame:CGRectMake(5, 0.5 , 44, 44)];//yubo
    //        [self.btnBack setTitle:@"返回" forState:UIControlStateNormal];
    //    }else{
    //    }
    
    self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20,0,SCREEN_WIDTH-40,NavigationBar_HEIGHT)];
    self.lblTitle.text = self.title;
    
    //SLog(@"title is %@",self.strTitle);
    self.lblTitle.backgroundColor = [UIColor clearColor];
    self.lblTitle.textAlignment = NSTextAlignmentCenter;
    self.lblTitle.textColor = [UIColor whiteColor];
    self.lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:UILABEL_DEFAULT_FONT_SIZE];
    [self.topToolBarView addSubview:self.lblTitle];
}

- (void) viewDidLayoutSubviews {
    if ([[[UIDevice currentDevice] systemVersion]floatValue]>= 7) {
        CGRect viewBounds = self.view.bounds;
        CGFloat topBarOffset = self.topLayoutGuide.length;
        viewBounds.origin.y = topBarOffset * -1;
        self.view.bounds = viewBounds;
    }
}


/*
 方法说明:
 添加并显示等待条
 
 参数说明:
 UIView* view        添加等待条的View对象
 BOOL    isAnimated  是否出现动画
 
 返回结果:
 void
 */
- (void)showProgressWithView:(UIView*)view animated:(BOOL)isAnimated{
    [self hideProgress:view animated:NO];
    
    MBProgressHUD *loading = [MBProgressHUD showHUDAddedTo:view animated:isAnimated];
    loading.mode=MBProgressHUDModeIndeterminate;
    loading.color = [UIColor lightGrayColor];
    
    _hideProgressTimer = [NSTimer scheduledTimerWithTimeInterval:20.0f
                                                          target:self
                                                        selector:@selector(handleTimer:)
                                                        userInfo:view
                                                         repeats:NO];
}
/*
 方法说明:
 隐藏并释放等待条
 
 参数说明:
 UIView* view        隐藏并释放等待条的View对象
 BOOL    isAnimated  是否出现动画
 
 返回结果:
 void
 */
- (void)hideProgress:(UIView*)view animated:(BOOL)isAnimated{
    
    if (nil != _hideProgressTimer && [_hideProgressTimer isValid]) {
        [_hideProgressTimer invalidate];
        //        [_hideProgressTimer release];
        _hideProgressTimer = nil;
    }
    [MBProgressHUD hideHUDForView:view animated:isAnimated];
    
}

/*
 方法说明:
 定时隐藏并释放等待条
 
 参数说明:
 定时器对象
 
 返回结果:
 void
 */
- (void)handleTimer:(NSTimer *)timer {
    
    [self hideProgress:[timer userInfo] animated:YES];
}
- (void)btnBackClicked:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
