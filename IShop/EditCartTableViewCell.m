

#import "EditCartTableViewCell.h"

@implementation EditCartTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.btnSelect setImage:IMAGE(@"xz_icon_nor") forState:UIControlStateNormal];
    [self.btnSelect setImage:IMAGE(@"xz_icon_pres") forState:UIControlStateSelected];
    
    self.txtCount.layer.borderColor = UIColorFromRGB(0xdbdbdb).CGColor;
    self.txtCount.layer.borderWidth = 1.0;
    self.txtCount.font = [UIFont systemFontOfSize:14];
    self.txtCount.placeholder = @"number";
    self.txtCount.textColor = UIColorFromRGB(0x333333);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
