
#import "PersonalCenterVC.h"
#import "PersonalCenterTableViewCell.h"
#import "LoginVC.h"
#import "RegisterVC.h"
#import "AFHTTPRequestOperationManager.h"
@interface PersonalCenterVC ()

@end

@implementation PersonalCenterVC
- (void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"userName"];
    NSString *email = [defaults objectForKey:@"email"];
    self.label1Line2.text = userName;
    if(self.label1Line2.text.length == 0)
    {
        self.label1Line2.text = @"userName";
    }
    self.label1Line2.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size = CGSizeMake(kWidth(160),kHeight(20));
    CGSize labelsize = [self.label1Line2.text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    self.label1Line2.frame = CGRectMake(98,kHeight(10), labelsize.width, kHeight(20));
    
    
    self.labelKind.frame = CGRectMake(labelsize.width + 107,kHeight(10), kWidth(65), kHeight(20));
    self.labelKind.hidden = YES;
    self.labelYuENum.text = [NSString stringWithFormat:@"￥%@",@"0"] ;
    self.labelJiFenNum.text = @"0";
    
    if(self.labelPhone.text.length == 0)
    {
        self.labelPhone.text = @"Consignee telephone";
        self.labelPhone.textColor = UIColorFromRGB(0x999999);
    }
    
    self.labelLocation.text = email;
    if(self.labelLocation.text.length == 0)
    {
        self.labelLocation.text = @"Dear, quick to fill out the address.";
        self.labelLocation.textColor = UIColorFromRGB(0x999999);
    }
    
    self.labelLocation.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize size1 = CGSizeMake(kWidth(290), kHeight(40));
    CGSize labelsize1 = [self.labelLocation.text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:size1 lineBreakMode:NSLineBreakByCharWrapping];
    self.labelLocation.frame = CGRectMake(kWidth(6), kHeight(96), labelsize1.width, labelsize1.height);
    if (userName.length !=0)
    {
        self.viewBg.hidden = NO;
       
    }else{
        self.viewBg.hidden = YES;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.text = @"Account Center";
    self.btnBack.hidden = YES;
    self.view.backgroundColor = UIColorFromRGB(0xf3f3f3);
//
    
    
    //tableHeaderView
    UIView *viewHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kWidth(320),kHeight(118))];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:viewHeadView.frame];
    imgView.image = IMAGE(@"");
    [viewHeadView addSubview:imgView];
    viewHeadView.backgroundColor = kWhiteColor;

 
    
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLogin.frame =CGRectMake(kWidth(110), kHeight(45), kWidth(88.5), kHeight(30));
    [btnLogin setTitle:@"Login" forState:UIControlStateNormal];
    btnLogin.titleLabel.font = [UIFont systemFontOfSize:15];
    btnLogin.tag = 101;
    [btnLogin addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLogin setBackgroundColor:kBackGrColor];
    btnLogin.layer.cornerRadius = 5;
    btnLogin.clipsToBounds = YES;

    [viewHeadView addSubview:btnLogin];
    
    UIButton *btnRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRegister.frame = CGRectMake(kWidth(108), kHeight(92), kWidth(100), kHeight(20));
    [btnRegister setTitle:@"Registered＞" forState:UIControlStateNormal];
    btnRegister.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnRegister setTitleColor:kBackGrColor forState:UIControlStateNormal];
    
    btnRegister.tag = 102;
    [btnRegister addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeadView addSubview:btnRegister];
   
    [self.view addSubview:viewHeadView];

//    
    UIButton *btnUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
    btnUpdate.frame = CGRectMake(kWidth(10), kHeight(310), kWidth(300), 44);
    [btnUpdate setTitle:@"Version update" forState:UIControlStateNormal];
    btnUpdate.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnUpdate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnUpdate setBackgroundColor:kBackGrColor];
    [btnUpdate setBackgroundColor:kBackGrColor];
    btnUpdate.tag = 102;
    [btnUpdate addTarget:self action:@selector(updateClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnUpdate];
    
    UIButton *btnLogOut = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLogOut.frame = CGRectMake(kWidth(10), kHeight(310) +55, kWidth(300), 44);
    [btnLogOut setTitle:@"Log out" forState:UIControlStateNormal];
    btnLogOut.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnLogOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLogOut setBackgroundColor:kBackGrColor];
    btnLogOut.tag = 102;
    [btnLogOut addTarget:self action:@selector(btnLogOutClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnLogOut];
    [self backView];
}
-(void)backView{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"userName"];
    NSString *email = [defaults objectForKey:@"email"];
    self.viewBg = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kWidth(320), kHeight(118))];
    self.viewBg.backgroundColor  = kWhiteColor;
    [self.view addSubview:self.viewBg];
    
    UILabel *label1Line1 = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(6),kHeight(10), kWidth(90), kHeight(20))];
    label1Line1.text = @"Hello! Dear";
    label1Line1.textColor = UIColorFromRGB(0x333333);
    label1Line1.backgroundColor = kClearColor;
    label1Line1.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:label1Line1];
    
    self.label1Line2 = [[UILabel alloc]init];
    self.label1Line2.textAlignment = NSTextAlignmentLeft;
    self.label1Line2.textColor = UIColorFromRGB(0x333333);
    self.label1Line2.backgroundColor = kClearColor;
    self.label1Line2.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:self.label1Line2];
    
    self.labelKind = [[UILabel alloc]init];
    self.labelKind.layer.cornerRadius = 5.0;
    self.labelKind.layer.masksToBounds = YES;
    self.labelKind.textColor = kWhiteColor;
    self.labelKind.textAlignment = NSTextAlignmentCenter;
    self.labelKind.backgroundColor = UIColorFromRGB(0xfacd89);
    self.labelKind.font = [UIFont systemFontOfSize:12];
    [self.viewBg addSubview:self.labelKind];
    
    UILabel *labelYuE = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(6),kHeight(33), 130, kHeight(20))];
    labelYuE.text = @"Account balance:";
    labelYuE.textColor = UIColorFromRGB(0x333333);
    labelYuE.backgroundColor = kClearColor;
    labelYuE.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:labelYuE];
    
    self.labelYuENum = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(6) +130,kHeight(33), kWidth(200), kHeight(20))];
    self.labelYuENum.textColor = kBackGrColor;
    self.labelYuENum.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:self.labelYuENum];
    
    UILabel *labelJiFen = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(6), kHeight(55), kWidth(100), kHeight(20))];
    labelJiFen.text = @"Loyal point:";
    labelJiFen.textColor = UIColorFromRGB(0x333333);
    labelJiFen.backgroundColor = kClearColor;
    labelJiFen.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:labelJiFen];
    
    self.labelJiFenNum = [[UILabel alloc]initWithFrame:CGRectMake(100, kHeight(55), kWidth(200), kHeight(20))];
    self.labelJiFenNum.textColor = kBackGrColor;
    self.labelJiFenNum.font = [UIFont systemFontOfSize:15];
    [self.viewBg addSubview:self.labelJiFenNum];
    
    UIImageView *imageViewLocation = [[UIImageView alloc]initWithImage:IMAGE(@"id_pic")];
    imageViewLocation.frame = CGRectMake(kWidth(4), kHeight(78), kWidth(16), kHeight(16));
    [self.viewBg addSubview:imageViewLocation];
    [self.tabelView reloadData];


   
}
-(void)updateClick:(NSNotification *)notification{
    [self upDataClick];
}
-(void)btnLogOutClick:(NSNotification *)notification{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"userName"];
    if (userName.length == 0) {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"You are not logged on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alt show];
    }
    else{
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Are you sure you want to quit it?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [alt show];
    }
}


-(void)buttonOnClick:(UIButton *)btn
{
    if (btn.tag == 101) {
        SaintiLog(@"101登录");
        LoginVC *loginVC = [[LoginVC alloc]init];
        [self.navigationController pushViewController:loginVC animated:YES];
    }else if (btn.tag == 102)
    {
        SaintiLog(@"102注册");
        RegisterVC *registerVC = [[RegisterVC alloc]init];
        [self.navigationController pushViewController:registerVC animated:YES];
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"userName"];
    switch (indexPath.section) {
        case 0:
        {
            if(userName != 0)
            {
                
            }else if (userName.length == 0)
            {
                LoginVC *loginVC = [[LoginVC alloc]init];
                [self.navigationController pushViewController:loginVC animated:YES];
            }
        }
            break;
        case 1:
        {
            if(userName.length != 0)
            {
               
            }else if(userName.length == 0)
            {
                LoginVC *loginVC = [[LoginVC alloc]init];
                [self.navigationController pushViewController:loginVC animated:YES];
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {

            }else if(indexPath.row == 1)
            {

            }else if(indexPath.row == 2)
            {

            }
        }
            break;
        case 3:
        {
            if (userName.length == 0) {
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"You are not logged on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alt show];
            }
            else{
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Are you sure you want to quit it?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
                [alt show];
            }
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        switch (buttonIndex) {
            case 0:
            {
                
            }
                break;
            case 1:
            {
                NSString *appStoreLink = [NSString stringWithFormat:@"http://itunes.apple.com/cn/app/id%@",APPID];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
            }
                break;
            default:
                break;
        }

    }else{
        if (buttonIndex == 0) {
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            [defaults removeObjectForKey:@"gesturePassword"];
            [defaults setObject:@"" forKey:@"userName"];
            [defaults setObject:@"" forKey:@"email"];
            [defaults synchronize];
            self.viewBg.hidden = YES;
            
        }
 
    }
    
}


-(void)upDataClick
{
    NSString *appleID = APPID;
    NSMutableString *requestUrlString = [[NSMutableString alloc] init];
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    //CFShow((__bridge CFTypeRef)(infoDic));
    NSString *currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    [requestUrlString appendFormat:@"http://itunes.apple.com/lookup"];
    [requestUrlString appendFormat:@"?id=%@", appleID];
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *country = [currentLocale objectForKey:NSLocaleCountryCode];
    if (country)
    {
        [requestUrlString appendFormat:@"&country=%@", country];
    }
    [self showProgressWithView:self.view animated:YES];
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:requestUrlString]];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    [self hideProgress:self.view animated:YES];
   
    NSArray *infoArray = [dic objectForKey:@"results"];
    if ([infoArray count]) {
        NSDictionary *releaseInfo = [infoArray objectAtIndex:0];
        NSString *lastVersion = [releaseInfo objectForKey:@"version"];
        
        float last = [lastVersion floatValue];
        float new = [currentVersion floatValue];
        if (new < last) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update" message:@"A new version of the update, whether to update？" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil];
            alert.delegate = self;
            alert.tag = 10000;
            [alert show];
        }else{
            [[CommonUtils sharedCommonUtils] showAlert:@"You have the latest version!" delegate:nil];
        }
        
    }else{
        [[CommonUtils sharedCommonUtils] showAlert:@"No updates detected！" delegate:nil];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
