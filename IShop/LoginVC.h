

#import "BaseViewController.h"

@interface LoginVC : BaseViewController <UITextFieldDelegate>

@property (retain, nonatomic) UITextField *textFieldUsrName;
@property (retain, nonatomic) UITextField *textFieldPassWd;

@end
