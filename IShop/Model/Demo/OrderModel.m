
#import "OrderModel.h"

@implementation OrderModel
-(id)initWithDic:(NSDictionary *)data{
    self=[super init];
    if (self) {
        self.coin=[data objectForKey:@"coin"];
        self.count=[data objectForKey:@"count"];
        self.date=[data objectForKey:@"date"];
        self.name=[data objectForKey:@"name"];
        self.orderId=[data objectForKey:@"orderId"];
        self.pic_url=[data objectForKey:@"pic_url"];
    }
     return self;
}
@end
