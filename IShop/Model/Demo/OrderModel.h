
#import <Foundation/Foundation.h>
#import "BaseModel.h"
@interface OrderModel : BaseModel
@property(nonatomic,strong) NSString *coin;
@property(nonatomic,strong) NSString *count;
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *orderId;
@property(nonatomic,strong) NSString *pic_url;
@end
