

#import "BaseModel.h"
@interface GoodsModel : BaseModel
@property(nonatomic,strong) NSString *deal_id;  //商品编号
@property(nonatomic,strong) NSString *title;  //商品编号（品号）
@property(nonatomic,strong) NSString *description1;//商品id
@property(nonatomic,strong) NSString *list_price;//商品名称
@property(nonatomic,strong) NSString *current_price;//上上个月数量
@property(nonatomic,strong) NSString *s_image_url;//上个月数量
@property(nonatomic,strong) NSString *publish_date;//.本月推荐
@property(nonatomic,strong) NSString *purchase_deadline;//规格
@property(nonatomic,strong) NSString *goods_num;//购买数量
@property(nonatomic,strong) NSString *totalPrice;//规格
@property(nonatomic,assign) BOOL isSelected;//
@end
