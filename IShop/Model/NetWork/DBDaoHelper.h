

#import <Foundation/Foundation.h>

@interface DBDaoHelper : NSObject

/*
 方法说明:
 创建数据库表,这个方法一般在启动程序时调用。appdelegate中调用
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL)createAllTable;
/*
 方法说明:
 插入购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )insertCartWith:(NSString *)deal_id title:(NSString *)title description:(NSString *)description list_price:(NSString *)list_price current_price:(NSString *)current_price s_image_url:(NSString *)s_image_url publish_date:(NSString *)publish_date purchase_deadline:(NSString *)purchase_deadline goods_num:(NSString *)goods_num;

/*
 方法说明:
 修改购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )updateCartWith:(NSString *)goods_id goods_num:(NSString *)goods_num;

/*
 方法说明:
 删除购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )deleteCartWith:(NSString *)goods_id;

/*
 方法说明:
 查询购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(NSArray *)selecCart;
/*
 方法说明:
 查询购物车商品数量
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(NSInteger )selecCartCount;

@end
