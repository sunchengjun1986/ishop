

#import "SendRequest.h"
#import "SaintiNetWork.h"

@implementation SendRequest
/*
 方法说明:
 用户登录
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)updateLoginWithUserName:(NSString *)userName result:(void (^)(NSDictionary *result, NSError *error))block{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"updateData" forKey:@"a"];
    //    NSString *str = [APService registrationID];
    //    if(str.length == 0){
    //        str = @"0a17fb78b8f";
    //    }
    NSString *sql = [NSString stringWithFormat:@"update iBeaconUser set login = 1,token='%@' where name='%@'",@"",userName];
    [dic setObject:sql forKey:@"sql"];
    return [[SaintiNetWork sharedClient]POST:@"index.php" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON){
        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(BaseModel *result, NSError *error){
        if (block) {
            block(nil, error);
        }
    }];
}

/*
 方法说明:
 AFNetWorking的POST请求实例
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord result:(void (^)(NSDictionary *result, NSError *error))block{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"selectData" forKey:@"a"];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM user WHERE 1 and user_name='%@' and user_pass='%@'",userName,passWord];
    [dic setObject:sql forKey:@"sql"];
    return [[SaintiNetWork sharedClient]POST:@"index.php" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON){
        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(BaseModel *result, NSError *error){
        if (block) {
            block(nil, error);
        }
    }];
    
}
/*
 方法说明:
 注册
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)registerWithUserName:(NSString *)userName passWord:(NSString *)passWord type:(NSString *)type result:(void (^)(NSDictionary *result, NSError *error))block{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"updateData" forKey:@"a"];
    
    
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO user(user_name, user_pass,address) VALUES ('%@','%@','%@')",userName,passWord,type];
    [dic setObject:sql forKey:@"sql"];
    return [[SaintiNetWork sharedClient]POST:@"index.php" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON){
        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(BaseModel *result, NSError *error){
        if (block) {
            block(nil, error);
        }
    }];
}
/*
 方法说明:
 用户是否存在
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)userWithUserName:(NSString *)userName result:(void (^)(NSDictionary *result, NSError *error))block{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:@"selectData" forKey:@"a"];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM user WHERE 1 and user_name='%@'",userName];
    [dic setObject:sql forKey:@"sql"];
    return [[SaintiNetWork sharedClient]POST:@"index.php" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON){
        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(BaseModel *result, NSError *error){
        if (block) {
            block(nil, error);
        }
    }];
}

@end
