

#import <Foundation/Foundation.h>
#import "OrderModel.h"
@interface SendRequest : NSObject
/*
 方法说明:
 用户登录
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)updateLoginWithUserName:(NSString *)userName result:(void (^)(NSDictionary *result, NSError *error))block;
/*
 方法说明:
 AFNetWorking的POST请求实例
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord result:(void (^)(NSDictionary *result, NSError *error))block;
/*
 方法说明:
 注册
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)registerWithUserName:(NSString *)userName passWord:(NSString *)passWord type:(NSString *)type result:(void (^)(NSDictionary *result, NSError *error))block;
/*
 方法说明:
 用户是否存在
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)userWithUserName:(NSString *)userName result:(void (^)(NSDictionary *result, NSError *error))block;
@end
