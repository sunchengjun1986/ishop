

#import "DBDaoHelper.h"
#import "DBHelper.h"
#import "GoodsModel.h"
@implementation DBDaoHelper
//创建数据库表
+(BOOL)createAllTable
{
    FMDatabase *db =[DBHelper openDatabase];
    BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS 'TABLE_Shopping_Cart'('cart_id'  INTEGER PRIMARY KEY AUTOINCREMENT,'deal_id' TEXT,'title' TEXT,'description' TEXT,'list_price' TEXT,'current_price' TEXT,'s_image_url' TEXT,'publish_date' TEXT,'purchase_deadline' TEXT,'goods_num' TEXT)"];
    
    [db close];
    if (result )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
/*
 方法说明:
 插入购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )insertCartWith:(NSString *)deal_id title:(NSString *)title description:(NSString *)description list_price:(NSString *)list_price current_price:(NSString *)current_price s_image_url:(NSString *)s_image_url publish_date:(NSString *)publish_date purchase_deadline:(NSString *)purchase_deadline goods_num:(NSString *)goods_num
{
    int goodNum = [goods_num integerValue];
    if (goodNum <= 0) {
        goods_num = @"1";
    }
    FMDatabase *db =[DBHelper openDatabase];
    FMResultSet *count = [db executeQuery:@"SELECT sum(goods_num) as count  FROM TABLE_Shopping_Cart WHERE deal_id = ?",deal_id];
    while (count.next)
    {
        int sum = [count intForColumn:@"count"];
        NSInteger all = [goods_num integerValue] + sum;
        if (all > 99999) {
            [db executeUpdate:@"UPDATE TABLE_Shopping_Cart SET goods_num = 99999 WHERE  and deal_id = ?",deal_id];
            NSString *str = [NSString stringWithFormat:@"INSERT INTO 'TABLE_Shopping_Cart'('deal_id','title','description','list_price','current_price','s_image_url','publish_date','purchase_deadline','goods_num') VALUES('%@','%@','%@','%@','%@','%@','%@','%@','99999')",deal_id,title,description,list_price,current_price,s_image_url,publish_date,purchase_deadline];
            [db executeUpdate:str];
            [db close];
            return YES;
        }
    }
    //执行查询语句
    FMResultSet *result = [db executeQuery:@"SELECT * FROM TABLE_Shopping_Cart WHERE deal_id = ?",deal_id];
    
    while (result.next)
    {
        //根据列名取得数据
        NSString *goods_numOld = [result stringForColumn:@"goods_num"];
        NSString *goods_deal_id = [result stringForColumn:@"deal_id"];
        if ([deal_id isEqual:goods_deal_id]) {
            NSInteger countNum = [goods_num integerValue] + [goods_numOld integerValue];
            BOOL resultUpdate = [db executeUpdate:@"UPDATE TABLE_Shopping_Cart SET goods_num = ? WHERE  deal_id = ?",[NSString stringWithFormat:@"%zd",countNum],deal_id];
            [db close];
            return resultUpdate;
            
        }
        
    }
    NSString *str = [NSString stringWithFormat:@"INSERT INTO 'TABLE_Shopping_Cart'('deal_id','title','description','list_price','current_price','s_image_url','publish_date','purchase_deadline','goods_num') VALUES('%@','%@','%@','%@','%@','%@','%@','%@','%@')",deal_id,title,description,list_price,current_price,s_image_url,publish_date,purchase_deadline,goods_num];
    BOOL resultInsert = [db executeUpdate:str];
    [db close];
    return resultInsert;
}

/*
 方法说明:
 修改购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )updateCartWith:(NSString *)goods_id goods_num:(NSString *)goods_num{
    int goodNum = [goods_num integerValue];
    if (goodNum <= 0) {
        goods_num = @"1";
    }else if (goodNum > 99999){
        goods_num = @"99999";
    }
    FMDatabase *db = [DBHelper openDatabase];
    
    BOOL result = [db executeUpdate:@"UPDATE TABLE_Shopping_Cart SET goods_num = ? WHERE deal_id = ?",goods_num,goods_id];
    [db close];
    return result;
}
/*
 方法说明:
 删除购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(BOOL )deleteCartWith:(NSString *)goods_id {
    FMDatabase *db = [DBHelper openDatabase];
    BOOL result = [db executeUpdate:@"DELETE FROM TABLE_Shopping_Cart WHERE deal_id = ?",goods_id];
    [db close];
    return result;
}
/*
 方法说明:
 查询购物车
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(NSArray *)selecCart{
    FMDatabase *db =[DBHelper openDatabase];
    //执行查询语句
    FMResultSet *result = [db executeQuery:@"SELECT * FROM TABLE_Shopping_Cart"];
    
    NSMutableArray *arrData = [[NSMutableArray alloc]init];
    while (result.next)
    {
        //根据列名取得数据
        NSString *deal_id = [result stringForColumn:@"deal_id"];
        NSString *title = [result stringForColumn:@"title"];
        NSString *description = [result stringForColumn:@"description"];
        NSString *list_price = [result stringForColumn:@"list_price"];
        NSString *current_price = [result stringForColumn:@"current_price"];
        NSString *s_image_url = [result stringForColumn:@"s_image_url"];
        NSString *publish_date = [result stringForColumn:@"publish_date"];
        NSString *purchase_deadline = [result stringForColumn:@"purchase_deadline"];
        NSString *goods_num = [result stringForColumn:@"goods_num"];
        
        GoodsModel *model = [[GoodsModel alloc]init];
        model.deal_id = deal_id;
        model.title = title;
        model.description1 = description;
        model.list_price = list_price;
        model.current_price = current_price;
        model.s_image_url = s_image_url;
        model.publish_date = publish_date;
        model.purchase_deadline = purchase_deadline;
        model.goods_num = goods_num;
        model.totalPrice = [NSString stringWithFormat:@"%.2f",[model.goods_num integerValue]*[model.current_price floatValue]];
        [arrData addObject:model];
    }
    [db close];
    return arrData;
    
}
/*
 方法说明:
 查询购物车商品数量
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
+(NSInteger )selecCartCount{
    FMDatabase *db =[DBHelper openDatabase];
    //执行查询语句
    FMResultSet *result = [db executeQuery:@"SELECT goods_num FROM TABLE_Shopping_Cart"];
    
    NSInteger count = 0;
    while (result.next)
    {
        
        count = count + [[result stringForColumn:@"goods_num"] integerValue];
    }
    [db close];
    return count;
    
}
@end
