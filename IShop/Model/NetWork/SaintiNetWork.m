

#import "SaintiNetWork.h"
//static NSString * const AFAppDotNetAPIBaseURLString = @"http://api.tv.58856.cn/index.php/";//get请求测试
static NSString * const AFAppDotNetAPIBaseURLString = @"http://yufeiyjm.sinaapp.com/";//post请求测试
@implementation SaintiNetWork
+ (instancetype)sharedClient {
    static SaintiNetWork *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[SaintiNetWork alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
//        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    });
    
    return _sharedClient;
}
@end
