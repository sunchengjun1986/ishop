//
//  SendRequest.m
//  SaintiFrameWork
//
//  Created by yan.jm on 14-5-6.
//  Copyright (c) 2014年 yan.jm. All rights reserved.
//

#import "SendRequest.h"
#import "SaintiNetWork.h"

@implementation SendRequest
+ (NSURLSessionDataTask *)orderDetailWith:(NSString *)userId result:(void (^)(OrderModel *result, NSError *error))block{

    return [[SaintiNetWork sharedClient] GET:[NSString stringWithFormat:@"gift/my_order?uid=%@",userId] parameters:nil success:^(NSURLSessionDataTask * __unused task, id JSON) {
        NSArray *dataArr = [JSON objectForKey:@"data"];
        OrderModel *model=[[OrderModel alloc]initWithDic:[dataArr objectAtIndex:0]];
        
        if (block) {//添加block返回
            block(model, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}
/*
 方法说明:
 AFNetWorking的POST请求实例
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord result:(void (^)(NSDictionary *result, NSError *error))block{
//    NSDictionary *dic=[[NSDictionary alloc]init];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:userName forKey:@"usr_name"];
     [dic setObject:passWord forKey:@"usr_pass"];
    return [[SaintiNetWork sharedClient]POST:@"tjstc_ydyyinternal_serverlet/LoginServlet" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON) {

        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
    
}
@end
