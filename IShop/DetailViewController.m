
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "DBDaoHelper.h"
@interface DetailViewController ()<DPRequestDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labDes;
@property (weak, nonatomic) IBOutlet UILabel *labPriceNew;
@property (weak, nonatomic) IBOutlet UILabel *labPriceOld;
@property (weak, nonatomic) IBOutlet UIButton *btnBuy;
@property (weak, nonatomic) IBOutlet UILabel *labTime;
@property (weak, nonatomic) IBOutlet UILabel *labContent;
@property (strong, nonatomic)  NSDictionary *dic;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *backScroll;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnBuy.layer.cornerRadius = 5.0;
    self.btnBuy.clipsToBounds = YES;
    [self queryAction:self.strGood_id];
    
}
- (IBAction)buyClick:(id)sender {
    BOOL result = [DBDaoHelper insertCartWith:self.strGood_id title:[self.dic objectForKey:@"title"] description:[self.dic objectForKey:@"description"] list_price:[NSString stringWithFormat:@"%@",[self.dic objectForKey:@"list_price"]] current_price:[NSString stringWithFormat:@"%@",[self.dic objectForKey:@"current_price"]] s_image_url:[self.dic objectForKey:@"s_image_url"] publish_date:[self.dic objectForKey:@"publish_date"] purchase_deadline:[self.dic objectForKey:@"purchase_deadline"] goods_num:@"1"];
    if (!result) {
        
        [[CommonUtils sharedCommonUtils]showAlert:@"A maximum of 99999 parts" delegate:nil];
    }else{
        
        [[CommonUtils sharedCommonUtils]showAlert:@"Success" delegate:nil];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    self.backScroll.contentSize = CGSizeMake(0, 600);
}
- (IBAction)queryAction:(NSString *)deal_id {
    
    [[AppDelegate instance] setAppKey:@"82754111"];
    [[AppDelegate instance] setAppSecret:@"725c0e45bcf34ab08520439f72372cb0"];
    
    NSString *url = @"v1/deal/get_single_deal";
    if (deal_id.length>0) {
        NSString *params = [NSString stringWithFormat:@"deal_id=%@",deal_id];
        [[[AppDelegate instance] dpapi] requestWithURL:url paramsString:params delegate:self];
    }else{
        NSString *params = [NSString stringWithFormat:@"deal_id=%@",deal_id];
        [[[AppDelegate instance] dpapi] requestWithURL:url paramsString:params delegate:self];
    }
    
}
- (void)request:(DPRequest *)request didFailWithError:(NSError *)error {
    
}

- (void)request:(DPRequest *)request didFinishLoadingWithResult:(id)result {
    if ([[result objectForKey:@"status"] isEqual:@"ERROR"]) {
        [[CommonUtils sharedCommonUtils]showAlert:[[result objectForKey:@"error"]objectForKey:@"errorMessage"] delegate:nil];
    }else{
        NSArray *arr = [result objectForKey:@"deals"];
        if (arr.count>0) {
            self.dic = [[result objectForKey:@"deals"] objectAtIndex:0];
            self.labTitle.text = [self.dic objectForKey:@"title"];
            self.labDes.text = [self.dic objectForKey:@"description"];
            self.labPriceNew.text= [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"current_price"]];
            self.labPriceOld.text= [NSString stringWithFormat:@" ￥%@",[self.dic objectForKey:@"list_price"]];
            self.labTime.text =[NSString stringWithFormat:@"%@ to %@",[self.dic objectForKey:@"publish_date"],[self.dic objectForKey:@"purchase_deadline"]];
            self.labContent.text= [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"details"]];
            
            
            NSArray *imageArr =[self.dic objectForKey:@"more_image_urls"];
            if (imageArr.count == 0) {
                UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 180)];
                [img setImage:IMAGE(@"defaultPic")];
                [self.imageScroll addSubview:img];
                
            }else{
                for (NSInteger i = 0; i < imageArr.count; i++) {
                    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenWidth*i, 0, kScreenWidth, 180)];
                    [img setImageWithURL:[NSURL URLWithString:[imageArr objectAtIndex:i] ] placeholderImage:IMAGE(@"defaultPic")];
                    [self.imageScroll addSubview:img];
                }
                self.imageScroll.contentSize = CGSizeMake(kScreenWidth*imageArr.count, 0);
            }

            
        }
       
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
