

#import "LoginVC.h"
#import "RegisterVC.h"
#import "NSString+Expand.h"
#import "SendRequest.h"
@interface LoginVC ()

@end

@implementation LoginVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.text = @"Login";
    self.btnBack.hidden = NO;
    self.view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    //type in user
    UIView *viewUserNameBG = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1), kHeight(64+self.topToolBarView.frame.size.height), kWidth(322), kHeight(49))];
    viewUserNameBG.backgroundColor = kWhiteColor;
    viewUserNameBG.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    viewUserNameBG.layer.borderWidth = 1.0;
    [self.view addSubview:viewUserNameBG];
    
    UIImageView *imageViewUserName = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth(42), kHeight(17), kWidth(16), kHeight(16))];
    imageViewUserName.image = IMAGE(@"用户名pic");
    [viewUserNameBG addSubview:imageViewUserName];
    
    self.textFieldUsrName = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(70), 0, kWidth(240), kHeight(49))];
    self.textFieldUsrName.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldUsrName.font = [UIFont systemFontOfSize:15];
    self.textFieldUsrName.delegate = self;
    self.textFieldUsrName.text = @"";
    self.textFieldUsrName.placeholder = @"USERNAME";
    self.textFieldUsrName.textColor = UIColorFromRGB(0x333333);
    [viewUserNameBG addSubview:self.textFieldUsrName];
    
    //password enter
    UIView *viewPassWd = [[UIView alloc]initWithFrame:CGRectMake(kWidth(-1),kHeight(125 + self.topToolBarView.frame.size.height), kWidth(322), kHeight(49))];
    viewPassWd.backgroundColor = kWhiteColor;
    viewPassWd.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    viewPassWd.layer.borderWidth = 1.0;
    [self.view addSubview:viewPassWd];
    
    UIImageView *imageViewPassWd = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth(42), kHeight(17), kWidth(16), kHeight(16))];
    imageViewPassWd.image = IMAGE(@"密码pic");
    [viewPassWd addSubview:imageViewPassWd];
    
    self.textFieldPassWd = [[UITextField alloc]initWithFrame:CGRectMake(kWidth(70), 0, kWidth(240), kHeight(49))];
    self.textFieldPassWd.font = [UIFont systemFontOfSize:15];
    self.textFieldPassWd.delegate = self;
    self.textFieldPassWd.text = @"";
    self.textFieldPassWd.secureTextEntry = YES;
    self.textFieldPassWd.clearButtonMode = UITextFieldViewModeAlways;
    self.textFieldPassWd.placeholder = @"Password";
    self.textFieldPassWd.textColor = UIColorFromRGB(0x333333);
    [viewPassWd addSubview:self.textFieldPassWd];
    
    //login button
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setTitle:@"Login" forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.tag = 103;
    btnLogin.layer.cornerRadius = 4.0;
    btnLogin.backgroundColor =kBackGrColor;
    [btnLogin setTitleColor:kWhiteColor forState:UIControlStateNormal];
    btnLogin.titleLabel.font = [UIFont systemFontOfSize:15];
    btnLogin.frame = CGRectMake(kWidth(15), kHeight(260 + self.topToolBarView.frame.size.height), kWidth(290),kHeight(44));
    [self.view addSubview:btnLogin];
    
    //no account?
    UILabel *labelRegister = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(70) , [UIScreen mainScreen].bounds.size.height-110, kWidth(130), kHeight(14))];
    labelRegister.text = @"Don’t have account?";
    labelRegister.textColor = UIColorFromRGB(0x333333);
    labelRegister.backgroundColor = kClearColor;
    labelRegister.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:labelRegister];

    //apply now
    UIButton *btnRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRegister setTitle:@"Apply now" forState:UIControlStateNormal];
    btnRegister.tag = 102;
    [btnRegister addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    btnRegister.backgroundColor = kClearColor;
    [btnRegister setTitleColor:kBackGrColor forState:UIControlStateNormal];
    btnRegister.titleLabel.font = [UIFont systemFontOfSize:12];
    btnRegister.frame = CGRectMake(kWidth(185), [UIScreen mainScreen].bounds.size.height-110, kWidth(60), kHeight(14));
    [self.view addSubview: btnRegister];
    
    UIView *viewLineRegister = [[UIView alloc]initWithFrame:CGRectMake(0, kHeight(13), kWidth(60),kHeight(1))];
    viewLineRegister.backgroundColor = kBackGrColor;
    [btnRegister addSubview:viewLineRegister];
    

}

- (void)buttonOnClick:(UIButton *)btn
{
    if (btn.tag == 101) {

    }else if (btn.tag == 102)
    {
        RegisterVC *registerVC = [[RegisterVC alloc]init];
        [self.navigationController pushViewController:registerVC animated:YES];
    }
    else if (btn.tag == 103)
    {
        if (self.textFieldUsrName.text.length == 0) {
            UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter your USERNAME" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
        }else if (self.textFieldPassWd.text.length == 0) {
            UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter your password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
        }
        else{
            [self showProgressWithView:self.view animated:YES];
            
            [SendRequest loginWithUserName:self.textFieldUsrName.text passWord:self.textFieldPassWd.text result:^(NSDictionary *result, NSError *error) {
                [self hideProgress:self.view animated:YES];
                if ([[result objectForKey:@"errorCode"] isEqualToString:@"1"]) {
                    
                    NSArray *data = [result objectForKey:@"data"];
                    if (data.count>0) {
                       ;
                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                        [defaults setObject:self.textFieldUsrName.text forKey:@"userName"];
                        [defaults setObject: [[data objectAtIndex:0] objectForKey:@"address"] forKey:@"email"];
                        [defaults synchronize];
                         [self.navigationController popViewControllerAnimated:YES];
                    }

                }else{
                    [[CommonUtils sharedCommonUtils]showAlert:@"登录失败" delegate:nil];
                }
                
            }];
            

        }
    }
}

-(BOOL) isValidateMobile:(NSString *)mobile
{
   
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(14[0,0-9])|(18[0,0-9])|(17[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
       return [phoneTest evaluateWithObject:mobile];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:NO];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
