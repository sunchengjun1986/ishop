

#import "AppDelegate.h"
#import "DBDaoHelper.h"
#import "ViewController.h"
#import "SearchViewController.h"
#import "ShoppingCartVC1.h"
#import "PersonalCenterVC.h"
#import "YLInitSwipePasswordController.h"
#import "YLCheckToUnlockViewController.h"
@implementation AppDelegate{
    UIViewController *viewController;
}
+ (AppDelegate *)instance {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (id)init {
    self = [super init];
    if (self) {
        _dpapi = [[DPAPI alloc] init];
        _appKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"appkey"];
        if (_appKey.length<1) {
            _appKey = kDPAppKey;
        }
        _appSecret = [[NSUserDefaults standardUserDefaults] valueForKey:@"appsecret"];
        if (_appSecret.length<1) {
            _appSecret = kDPAppSecret;
        }
    }
    return self;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    SaintiLog(@"%d-%d-%d",1,2,3);
//    if ([[[UIDevice currentDevice] systemVersion]floatValue]>= 7) {
//        [application setStatusBarStyle:UIStatusBarStyleLightContent];
//    }
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YY-MM-dd HH:mm"];
    NSDate *old = [dateformatter dateFromString:@"2015-01-03 07:40"];
    NSLog(@"%@",[CommonUtils messageTimeWithDate:old]);
    //Create database
    [DBDaoHelper createAllTable];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


    
    SearchViewController *HomePage = [[SearchViewController alloc]init];
    UINavigationController *navHomePage = [[UINavigationController alloc]initWithRootViewController:HomePage];
    navHomePage.navigationBarHidden = YES;
    HomePage.title = @"Home";
    
    
    ShoppingCartVC1 *ShoppingCart = [[ShoppingCartVC1 alloc]init];
    UINavigationController *navShoppingCart = [[UINavigationController alloc]initWithRootViewController:ShoppingCart];
    navShoppingCart.navigationBarHidden = YES;
    ShoppingCart.title = @"Shopping Cart";
    
    PersonalCenterVC *PersonalCenter = [[PersonalCenterVC alloc]init];
    UINavigationController *navPersonalCenter = [[UINavigationController alloc]initWithRootViewController:PersonalCenter];
    navPersonalCenter.navigationBarHidden = YES;
    PersonalCenter.title = @"Account Center";
    
    [navHomePage.tabBarItem setImage:IMAGE(@"tab_boddy_nor")];
    [navHomePage.tabBarItem setSelectedImage:IMAGE(@"tab_boddy_pres")];
    
    [navShoppingCart.tabBarItem setImage:IMAGE(@"tab_gwc_nor")];
    [navShoppingCart.tabBarItem setSelectedImage:IMAGE(@"tab_gwc_pres")];
    
    [navPersonalCenter.tabBarItem setImage:IMAGE(@"tab_grzx_nor")];
    [navPersonalCenter.tabBarItem setSelectedImage:IMAGE(@"tab_grzx_pres")];
    
    
    
    UITabBarController *tabar = [[UITabBarController alloc] init];
    tabar.viewControllers = [NSArray arrayWithObjects:navHomePage, navShoppingCart,navPersonalCenter, nil];
    
    [tabar setSelectedIndex:0];
    tabar.tabBar.tintColor = kBackGrColor;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:tabar];
    [nav setNavigationBarHidden:YES];
    
    
    
    

    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
   
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    if (userName.length>0) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"gesturePassword"]) {
            YLCheckToUnlockViewController *controller = [YLCheckToUnlockViewController new];
            [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
        }else {
            YLInitSwipePasswordController *controller = [YLInitSwipePasswordController new];
            [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    if (!_isFirst) {
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
        if (userName.length>0) {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"gesturePassword"]) {
                YLCheckToUnlockViewController *controller = [YLCheckToUnlockViewController new];
                [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
            }else {
                YLInitSwipePasswordController *controller = [YLInitSwipePasswordController new];
                [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
            }
        }
        _isFirst = YES;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}
- (void)setAppKey:(NSString *)appKey {
    _appKey = appKey;
    [[NSUserDefaults standardUserDefaults] setValue:appKey forKey:@"appkey"];
}

- (void)setAppSecret:(NSString *)appSecret {
    _appSecret = appSecret;
    [[NSUserDefaults standardUserDefaults] setValue:appSecret forKey:@"appsecret"];
}
@end
