

#import "OrderConfirmViewController.h"
#import "UIImageView+AFNetworking.h"
#import "DBDaoHelper.h"
#import "GoodsModel.h"
#import <LocalAuthentication/LocalAuthentication.h>
@interface OrderConfirmViewController (){
    float sum;
    int count;
}
//@property (strong, nonatomic)  OrderModel *order;
//@property (strong, nonatomic)  ReceiverModel *receiverModel;
//个人信息
@property (weak, nonatomic) IBOutlet UIImageView *imageBack;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labPhone;
@property (weak, nonatomic) IBOutlet UILabel *labAddress;

//商品信息
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet UILabel *labGoodsCount;

//支付方式
@property (strong, nonatomic) IBOutlet UIButton *btnZaixian;
@property (strong, nonatomic) IBOutlet UIButton *btnXianjin;
@property (strong, nonatomic) IBOutlet UIButton *btnPos;
@property (strong, nonatomic) IBOutlet UIButton *btnZhuanzhang;

//使用余额
@property (weak, nonatomic) IBOutlet UILabel *labBalanceStart;
@property (weak, nonatomic) IBOutlet UILabel *labBalanceEnd;
@property (weak, nonatomic) IBOutlet UISwitch *switchBalance;

//总计
@property (weak, nonatomic) IBOutlet UILabel *labTotalStart;
@property (weak, nonatomic) IBOutlet UILabel *labTotalEnd;
@property (weak, nonatomic) IBOutlet UILabel *labYunfeiStart;
@property (weak, nonatomic) IBOutlet UILabel *labYunfeiEnd;
@property (weak, nonatomic) IBOutlet UILabel *labDongjieStart;
@property (weak, nonatomic) IBOutlet UILabel *labDongjieEnd;
@property (weak, nonatomic) IBOutlet UILabel *labYingfuStart;
@property (weak, nonatomic) IBOutlet UILabel *labYingfuEnd;

//合计
@property (weak, nonatomic) IBOutlet UILabel *labZongEStart;
@property (weak, nonatomic) IBOutlet UILabel *labZongEEnd;
@property (weak, nonatomic) IBOutlet UILabel *labZongJian;
@property (weak, nonatomic) IBOutlet UILabel *labZongDai;


@end

@implementation OrderConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnBack.hidden = NO;
    self.lblTitle.text = @"Confirmation of order";
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:kColor(240, 240, 240, 1)];
    
    UIView *viewStatusBarBk = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 20)];
    viewStatusBarBk.backgroundColor = kBackGrColor;
    [self.view addSubview:viewStatusBarBk];
    if (iPhone6) {
        self.imageBack.image = IMAGE1(@"shdz_pic");
    }
    self.topToolBarView.frame = CGRectMake(0, 20, kScreenWidth, 44);
    self.btnZaixian.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZaixian.layer.borderWidth = 1;
    self.btnXianjin.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnXianjin.layer.borderWidth = 1;
    self.btnPos.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnPos.layer.borderWidth = 1;
    self.btnZhuanzhang.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZhuanzhang.layer.borderWidth = 1;
    self.strPayMentType = @"0";
    
}
-(void)viewDidAppear:(BOOL)animated{
    self.labName.text = @"CUSTOM";
    self.labPhone.text = @"CONTACT NUMBER";
    self.labAddress.text = @"PLEASE EDIT YOUR DELIVERY ADDRESS";
    sum = 0.0;
    count = 0;
    for (int i = 0; i<self.allList.count; i++) {
        GoodsModel *goodModel = [self.allList objectAtIndex:i];
        float money = [goodModel.totalPrice floatValue];
        count = count +[goodModel.goods_num integerValue] ;
        sum = sum +money;
        switch (i) {
            case 0:
            {
                [self.image1 setImageWithURL:[NSURL URLWithString:goodModel.s_image_url] placeholderImage:IMAGE(@"defaultPic")];
            }
                break;
            case 1:
            {
                [self.image2 setImageWithURL:[NSURL URLWithString:goodModel.s_image_url] placeholderImage:IMAGE(@"defaultPic")];
            }
                break;
            case 2:
            {
                [self.image3 setImageWithURL:[NSURL URLWithString:goodModel.s_image_url] placeholderImage:IMAGE(@"defaultPic")];
            }
                break;
            default:
                break;
        }
    }
    self.labGoodsCount.text = [NSString stringWithFormat:@"%zd",self.allList.count];
    self.btnZhuanzhang.hidden = YES;
    self.btnPos.hidden = YES;
    self.btnXianjin.hidden = YES;
    self.btnZaixian.hidden = NO;
    
    /*****余额 e*****/
    NSArray *arrPrice = [@"0.00" componentsSeparatedByString:@"."];
    self.labBalanceStart.text = [arrPrice objectAtIndex:0];
    if (arrPrice.count>1) {
        self.labBalanceEnd.text = [NSString stringWithFormat:@".%@",[arrPrice objectAtIndex:1]];
    }
    
    
    /*****总金额*****/
    NSArray *arrtotalPrice = [[NSString stringWithFormat:@"%.2f",sum] componentsSeparatedByString:@"."];
    self.labTotalStart.text = [arrtotalPrice objectAtIndex:0];
    if (arrtotalPrice.count>1) {
        self.labTotalEnd.text = [NSString stringWithFormat:@".%@",[arrtotalPrice objectAtIndex:1]];
    }
    
    
    NSArray *arrFreightPrice = [@"10.00" componentsSeparatedByString:@"."];
    self.labYunfeiStart.text = [arrFreightPrice objectAtIndex:0];
    if (arrFreightPrice.count>1) {
        self.labYunfeiEnd.text = [NSString stringWithFormat:@".%@",[arrFreightPrice objectAtIndex:1]];
    }
    
    NSArray *arrDongjiePrice = [@"0.00" componentsSeparatedByString:@"."];
    self.labDongjieStart.text = [arrDongjiePrice objectAtIndex:0];
    if (arrDongjiePrice.count>1) {
        self.labDongjieEnd.text = [NSString stringWithFormat:@".%@",[arrDongjiePrice objectAtIndex:1]];
    }
    
    
    
    float zongPrice = sum;
    
    float yingfuPrice  = zongPrice  + [@"10.00" floatValue] - [@"0.00" floatValue];
    
    
    NSArray *arrZongPrice = [[NSString stringWithFormat:@"%.2f",yingfuPrice] componentsSeparatedByString:@"."];
    self.labYingfuStart.text = [arrZongPrice objectAtIndex:0];
    if (arrZongPrice.count>1) {
        self.labYingfuEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
    }
    
    
    /*****总计*****/
    self.labZongEStart.text = [arrZongPrice objectAtIndex:0];
    if (arrZongPrice.count>1) {
        self.labZongEEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
    }
    if (count > 0) {
        self.labZongJian.text = [NSString stringWithFormat:@"%d",count];
    }
    
    
}
- (void)viewDidLayoutSubviews{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 方法说明:
 修改收货人地址
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
- (IBAction)updateAddressClick:(id)sender {

}
/*
 方法说明:
 商品清单点击
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
- (IBAction)allGoodsListClick:(id)sender {

    
}
/*
 方法说明:
 支付方式点击
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
- (IBAction)zaixianClick:(id)sender {
    self.strPayMentType = @"1";
    self.btnZaixian.layer.borderColor = kColor(41, 166, 227, 1).CGColor;
    self.btnZaixian.layer.borderWidth = 1;
    self.btnXianjin.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnXianjin.layer.borderWidth = 1;
    self.btnPos.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnPos.layer.borderWidth = 1;
    self.btnZhuanzhang.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZhuanzhang.layer.borderWidth = 1;
}
- (IBAction)xianjinClick:(id)sender {
    self.strPayMentType = @"2";
    self.btnZaixian.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZaixian.layer.borderWidth = 1;
    self.btnXianjin.layer.borderColor = kColor(41, 166, 227, 1).CGColor;
    self.btnXianjin.layer.borderWidth = 1;
    self.btnPos.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnPos.layer.borderWidth = 1;
    self.btnZhuanzhang.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZhuanzhang.layer.borderWidth = 1;
}
- (IBAction)posClick:(id)sender {
    self.strPayMentType = @"3";
    self.btnZaixian.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZaixian.layer.borderWidth = 1;
    self.btnXianjin.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnXianjin.layer.borderWidth = 1;
    self.btnPos.layer.borderColor = kColor(41, 166, 227, 1).CGColor;
    self.btnPos.layer.borderWidth = 1;
    self.btnZhuanzhang.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZhuanzhang.layer.borderWidth = 1;
}
- (IBAction)zhuanzhuangClick:(id)sender {
    self.strPayMentType = @"4";
    self.btnZaixian.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnZaixian.layer.borderWidth = 1;
    self.btnXianjin.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnXianjin.layer.borderWidth = 1;
    self.btnPos.layer.borderColor = kColor(181, 181, 181, 1).CGColor;
    self.btnPos.layer.borderWidth = 1;
    self.btnZhuanzhang.layer.borderColor = kColor(41, 166, 227, 1).CGColor;
    self.btnZhuanzhang.layer.borderWidth = 1;
}
/*
 方法说明:
 结算按钮点击
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
- (IBAction)jiesuanClick:(id)sender {
    
    if ([self.strPayMentType isEqualToString:@"0"]) {
        [[CommonUtils sharedCommonUtils] showAlert:@"Please select your payment method" delegate:nil];
    }else{
        NSString *useaccount = @"0";
        if (self.switchBalance.on) {
            useaccount = @"1";
        }else{
            useaccount = @"0";
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        
        LAContext *myContext = [[LAContext alloc] init];
        myContext.localizedFallbackTitle = @"";
        NSError *authError = nil;
        NSString *myLocalizedReasonString = @"Restricted AreaRestricted AreaRestricted AreaRestricted AreaRestricted AreaRestricted AreaRestricted Area!";
        
        if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
            [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                      localizedReason:myLocalizedReasonString
                                reply:^(BOOL success, NSError *error) {
                                    if (success) {
                                        // User authenticated successfully, take appropriate action
                                        
                                        [self performSelectorOnMainThread:@selector(clikss) withObject:nil waitUntilDone:YES];
                                        
                                    } else {
                                       
                                        // User did not authenticate successfully, look at error and take appropriate action
                                        NSString *str = @"Fail";
//                                        alert.title = @"Fail";
                                        
                                        switch (error.code) {
                                            case LAErrorUserCancel:
                                                str = @"Authentication Cancelled";
                                                break;
                                                
                                            case LAErrorAuthenticationFailed:
                                                str = @"Authentication Failed";
                                                break;
                                                
                                            case LAErrorPasscodeNotSet:
                                                str = @"Passcode is not set";
                                                break;
                                                
                                            case LAErrorSystemCancel:
                                                str = @"System cancelled authentication";
                                                break;
                                                
                                            case LAErrorUserFallback:
                                                str = @"You chosed to try password";
                                                break;
                                                
                                            default:
                                                str = @"You cannot access to private content!";
                                                break;
                                        }
                                        [self performSelectorOnMainThread:@selector(clikssError:) withObject:str waitUntilDone:YES];
                                    }
                                }];
        } else {
            // Could not evaluate policy; look at authError and present an appropriate message to user
            alert.title = @"Warning";
            
            if(authError.code == LAErrorTouchIDNotEnrolled) {
                alert.message = @"You do not have any fingerprints enrolled!";
            }else if(authError.code == LAErrorTouchIDNotAvailable) {
                alert.message = @"Your device does not support TouchID authentication!";
            }else if(authError.code == LAErrorPasscodeNotSet){
                alert.message = @"Your passcode has not been set";
            }
            
            [alert show];
        }
    
    }
    
}
-(void)clikss{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    alert1.title = @"Success";
    alert1.message = @"You have pay success!";
    [alert1 show];
    
    for (int i = 0; i< self.allList.count; i++) {
        GoodsModel *goodModel  = [self.allList objectAtIndex:i];
        ;
        [DBDaoHelper deleteCartWith:goodModel.deal_id];
        
    }
}
-(void)clikssError:(id)obj{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    alert1.title = @"Error";
    alert1.message = obj;
    [alert1 show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    ;
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 方法说明:
 是否使用余额
 
 参数说明:
 <#参数说明#>
 
 返回结果:
 <#返回结果#>
 */
- (IBAction)switchChange:(UISwitch *)sender {
    float accountPrice = [@"0.00" floatValue];
    float zongPrice = sum;
    
    if (sender.on) {
        //        float dongjiePrice = [@"0.00" floatValue];
        float yingfuPrice = zongPrice;
        if (accountPrice >= (zongPrice + [@"10.00" floatValue] - [@"0.00" floatValue])) {
            //            dongjiePrice = zongPrice;
            
            yingfuPrice = accountPrice  - [@"10.00" floatValue] - zongPrice  + [@"0.00" floatValue];
            if (yingfuPrice > 0) {
                yingfuPrice = 0.00;
            }
        }else{
            //            dongjiePrice = accountPrice;
            yingfuPrice = zongPrice  + [@"10.00" floatValue] - accountPrice - [@"0.00" floatValue];
        }

        
        NSArray *arrZongPrice = [[NSString stringWithFormat:@"%.2f",yingfuPrice] componentsSeparatedByString:@"."];
        self.labYingfuStart.text = [arrZongPrice objectAtIndex:0];
        if (arrZongPrice.count>1) {
            self.labYingfuEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
        }
        
        
        /*****总计*****/
        self.labZongEStart.text = [arrZongPrice objectAtIndex:0];
        if (arrZongPrice.count>1) {
            self.labZongEEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
        }
        
    }else{
        float zongPrice = sum;
        
        float yingfuPrice  = zongPrice  + [@"10" floatValue] - [@"0" floatValue];
        
        
        NSArray *arrZongPrice = [[NSString stringWithFormat:@"%.2f",yingfuPrice] componentsSeparatedByString:@"."];
        self.labYingfuStart.text = [arrZongPrice objectAtIndex:0];
        if (arrZongPrice.count>1) {
            self.labYingfuEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
        }
        
        
        /*****总计*****/
        self.labZongEStart.text = [arrZongPrice objectAtIndex:0];
        if (arrZongPrice.count>1) {
            self.labZongEEnd.text = [NSString stringWithFormat:@".%@",[arrZongPrice objectAtIndex:1]];
        }
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
