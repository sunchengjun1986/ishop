

#import "SearchViewController.h"
#import "GoodsListTableViewCell.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "DetailViewController.h"
@interface SearchViewController ()<DPRequestDelegate>

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.btnBack.hidden = YES;
    self.tableView = [[PullToRefreshTableView alloc]initWithFrame:CGRectMake(kWidth(7),self.topToolBarView.frame.size.height + kHeight(45), kWidth(306), kScreenHeight-self.topToolBarView.frame.size.height-65)];
    _page = 1;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate_PullDown = self;
    self.tableView.delegate_PullUp = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    [self.view addSubview:self.tableView];
    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, self.topToolBarView.frame.size.height, kScreenWidth, kHeight(45))];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = self.strSearch;
    self.searchBar.delegate = self;

    
    [self.view addSubview:self.searchBar];
    
    self.tableView.frame = CGRectMake(kWidth(7),self.topToolBarView.frame.size.height+kHeight(45), kWidth(306), kScreenHeight-self.topToolBarView.frame.size.height-20-45);
     [self queryAction:self.searchBar.text];
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    if(self.searchBar.text.length == 0)
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please input you want to search content" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alt show];
    }else{
        self.strSearch = self.searchBar.text;
         [self queryAction:self.searchBar.text];
    }
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


#pragma mark


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *string = @"Cell";
    GoodsListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        cell = [[GoodsListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dic = [self.arrList objectAtIndex:indexPath.row];
    cell.labelName.text = [dic objectForKey:@"title"];
    cell.labelPriceYuan.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"current_price"]];
    [cell.imageViewIcon setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"s_image_url"]] placeholderImage:IMAGE(@"defaultPic")];
    cell.labelDanwei.text = [NSString stringWithFormat:@"%@至%@",[dic objectForKey:@"publish_date"],[dic objectForKey:@"purchase_deadline"]];
    return cell;
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView tableViewDidDragging];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableView tableViewDidEndDragging];
    
}

- (void)pullDownEvent{
    _page = 1;
     [self queryAction:self.searchBar.text];
}
- (void)pullUpEvent{
    _page ++;
     [self queryAction:self.searchBar.text];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kHeight(115);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSDictionary *dic = [self.arrList objectAtIndex:indexPath.row];
    DetailViewController *vc=[[DetailViewController alloc]init];
    vc.strGood_id = [dic objectForKey:@"deal_id"];
    [self.tabBarController.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)queryAction:(NSString *)keyWord {
    
    [[AppDelegate instance] setAppKey:@"82754111"];
    [[AppDelegate instance] setAppSecret:@"725c0e45bcf34ab08520439f72372cb0"];
    
    NSString *url = @"v1/deal/find_deals";
    if (keyWord.length>0) {
        NSString *params = [NSString stringWithFormat:@"city=上海&limit=10&page=%d&keyword=%@",_page,keyWord];
        [[[AppDelegate instance] dpapi] requestWithURL:url paramsString:params delegate:self];
    }else{
        NSString *params = [NSString stringWithFormat:@"city=上海&limit=10&page=%d",_page];
        [[[AppDelegate instance] dpapi] requestWithURL:url paramsString:params delegate:self];
    }
    
}
- (void)request:(DPRequest *)request didFailWithError:(NSError *)error {
    
}

- (void)request:(DPRequest *)request didFinishLoadingWithResult:(id)result {
    if ([[result objectForKey:@"status"] isEqual:@"ERROR"]) {
        [[CommonUtils sharedCommonUtils]showAlert:[[result objectForKey:@"error"]objectForKey:@"errorMessage"] delegate:nil];
    }else{
        NSArray *tempArr = [result objectForKey:@"deals"];
        self.arrList = [result objectForKey:@"deals"];
        if (_page == 1) {
            self.arrList = [[NSMutableArray alloc]initWithArray:tempArr];
            if (tempArr.count<10) {
                [self.tableView reloadData:YES];
            }else{
                [self.tableView reloadData:NO];
            }
        }else{
             [self.arrList addObjectsFromArray:tempArr];
            if (tempArr.count<10) {
                [self.tableView reloadData:YES];
            }else{
                [self.tableView reloadData:NO];
            }
        }
    }
    
}


@end
