/*
* 建立日期	： 2014-04-15
* 版权声明	： 本代码版权归圣笛科技所有，禁止任何未授权的传播和使用
* 作者		： yanjiaming@sainti.com
* 模块		：
* 描述		： 请求接口封装 DAO
* -------------------------------------------------------------------------
* 修改历史
* 序号			日期					修改人				修改原因
* <#序号#>        <#日期#>                <#修改人#>              <#修改原因#>
*
***************************************************************************
*/

#import "SendRequest.h"
#import "SaintiNetWork.h"
#import "OrderModel.h"
@implementation SendRequest
+ (NSURLSessionDataTask *)orderDetailWith:(NSString *)userId result:(void (^)(NetWorkModel *result, NSError *error))block{

    return [[SaintiNetWork sharedClient] GET:[NSString stringWithFormat:@"gift/my_order?uid=%@",userId] parameters:nil success:^(NSURLSessionDataTask * __unused task, id JSON) {
        NetWorkModel *resultModel=[[NetWorkModel alloc]init];
        resultModel.errorCode=[JSON objectForKey:@"result"];
        resultModel.message=[JSON objectForKey:@"msg"];
        NSArray *dataArr = [JSON objectForKey:@"data"];
        NSMutableArray *resultArr=[[NSMutableArray alloc]init];
        for (int i=0; i<dataArr.count; i++) {
            OrderModel *model=[[OrderModel alloc]initWithDic:[dataArr objectAtIndex:0]];
            [resultArr addObject:model];
        }
        
        resultModel.resultArr=resultArr;
        if (block) {//添加block返回
            block(resultModel, nil);
        }
    } failure:^(NetWorkModel *result, NSError *error) {
        if (block) {
            block(result, error);
        }
    }];
}
/*
 方法说明:
 AFNetWorking的POST请求实例
 
 参数说明:
 userName:请求参数
 passWord:
 返回结果:
 NSDictionary *result；block的返回结果，这个可以自定义，可以是数组，字典，也可以是一个实例类，建议实例类，这样用起来比较方便，代码易读。
 */
+ (NSURLSessionDataTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord result:(void (^)(NetWorkModel *result, NSError *error))block{
//    NSDictionary *dic=[[NSDictionary alloc]init];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:userName forKey:@"usr_name"];
     [dic setObject:passWord forKey:@"usr_pass"];
    return [[SaintiNetWork sharedClient]POST:@"tjstc_ydyyinternal_serverlet/LoginServlet" parameters:dic success:^(NSURLSessionDataTask * __unused task, id JSON) {

        if (block) {//添加block返回
            block(JSON, nil);
        }
    }failure:^(NetWorkModel *result, NSError *error) {
        if (block) {
            block(result, error);
        }
    }];
    
}
@end
