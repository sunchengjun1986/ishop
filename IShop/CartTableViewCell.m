
#import "CartTableViewCell.h"

@implementation CartTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.selectBtn setImage:IMAGE(@"xz_icon_nor") forState:UIControlStateNormal];
    [self.selectBtn setImage:IMAGE(@"xz_icon_pres") forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
