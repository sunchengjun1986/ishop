

#import <UIKit/UIKit.h>

@interface EditCartTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labPriceStar;
@property (weak, nonatomic) IBOutlet UILabel *labPriceEnd;
@property (weak, nonatomic) IBOutlet UITextField *txtCount;
@property (weak, nonatomic) IBOutlet UILabel *labUnit;
@end
