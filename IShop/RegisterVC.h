

#import "BaseViewController.h"

@interface RegisterVC : BaseViewController <UITextFieldDelegate , UIAlertViewDelegate>

@property (retain, nonatomic) UITextField *textFieldUsrName;
@property (retain, nonatomic) UITextField *textFieldVerifyCode;
@property (retain, nonatomic) UITextField *textFieldPassWd;
@property (retain, nonatomic) UITextField *textFieldVerifyPassWd;
@property (retain, nonatomic) UITextField *textFieldInvitedCode;
@property (retain, nonatomic) UIView *viewVerifyCode;
@property (retain, nonatomic) UIView *viewNormal;
@property (retain, nonatomic) UIButton *btnVerifyCode;
@property (retain, nonatomic) NSTimer *t;
@property (copy, nonatomic) NSString *strCodeExist;
@end
