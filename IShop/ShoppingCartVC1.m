
#import "ShoppingCartVC1.h"
#import "CartTableViewCell.h"
#import "EditCartTableViewCell.h"
#import "DBDaoHelper.h"
#import "UIImageView+AFNetworking.h"
#import "Common.h"
#import "GoodsModel.h"
#import "DetailViewController.h"
#import "LoginVC.h"
#import "OrderConfirmViewController.h"
@interface ShoppingCartVC1 ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)  UIButton *btnEdit;
@property (strong, nonatomic) NSMutableArray *arrCartList;
@property(strong,nonatomic) UITextField *txtField;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIView *deleteView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll1;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll2;
@property (weak, nonatomic) IBOutlet UILabel *labCount;
@property (weak, nonatomic) IBOutlet UILabel *labPriceStar;
@property (weak, nonatomic) IBOutlet UILabel *labPriceEnd;

@end

@implementation ShoppingCartVC1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.lblTitle.text = @"Shopping Cart";
    self.btnBack.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    
    [self.btnSelectAll1 setImage:IMAGE(@"xz_icon_nor") forState:UIControlStateNormal];
    [self.btnSelectAll1 setImage:IMAGE(@"xz_icon_pres") forState:UIControlStateSelected];
    
    [self.btnSelectAll2 setImage:IMAGE(@"xz_icon_nor") forState:UIControlStateNormal];
    [self.btnSelectAll2 setImage:IMAGE(@"xz_icon_pres") forState:UIControlStateSelected];
    
    UIView *viewStatusBarBk = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 20)];
    viewStatusBarBk.backgroundColor = kBackGrColor;
    [self.view addSubview:viewStatusBarBk];
    self.topToolBarView.frame = CGRectMake(0, 20, kScreenWidth, 44);
    self.btnDelete.layer.borderColor = UIColorFromRGB(0xf3ad44).CGColor;
    self.btnDelete.layer.borderWidth = 1;
    self.btnDelete.layer.cornerRadius = 3;
    

    self.tableView.backgroundColor = kColor(240, 240, 240, 1);
    
    self.btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnEdit addTarget:self action:@selector(editClick1:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnEdit.frame = CGRectMake(kWidth(250), 20, kWidth(70), 44);
    [self.view addSubview: self.btnEdit];
    self.btnEdit.titleLabel.font = k15Font;
    [self.btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    [self.btnEdit setTitle:@"Complete" forState:UIControlStateSelected];
}



-(void)viewDidAppear:(BOOL)animated{
     [self loadCartList];
}

- (void)loadCartList{
   

    NSArray *arrCar = [DBDaoHelper selecCart];
    
    self.btnSelectAll1.selected = NO;
    self.btnSelectAll2.selected = NO;
    
    self.arrCartList = [[NSMutableArray alloc]initWithArray:arrCar];
   
    NSInteger count = 0;
    float price = 0.0;
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == YES) {
            count = count + [model.goods_num integerValue];
            price = price + [model.totalPrice floatValue];
        }
    }
    NSArray *arrPrice = [[NSString stringWithFormat:@"%.2f",price] componentsSeparatedByString:@"."];
    self.labPriceStar.text = [arrPrice objectAtIndex:0];
    if (arrPrice.count>1) {
        self.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arrPrice objectAtIndex:1]];
    }
    self.labCount.text = [NSString stringWithFormat:@"%ld",(long)count];

    [_tableView reloadData];
}
- (void)viewDidLayoutSubviews{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.btnEdit.selected == NO){
        static NSString *adIfier = @"CartTableViewCell" ;
        CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:adIfier];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"CartTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        GoodsModel *model = [self.arrCartList objectAtIndex:indexPath.row];
        cell.selectBtn.selected = model.isSelected;
        cell.selectBtn.tag = indexPath.row + 100;
        [cell.selectBtn addTarget:self action:@selector(addOrderClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.imgMain setImageWithURL:[NSURL URLWithString:model.s_image_url] placeholderImage:IMAGE(@"defaultPic")];
        cell.labTitle.text = model.title;
        cell.labCount.text = model.goods_num;
        ;
        NSArray *arr = [model.totalPrice componentsSeparatedByString:@"."];
        cell.labPriceStar.text = [arr objectAtIndex:0];
        if (arr.count>1) {
            cell.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arr objectAtIndex:1]];
        }
//        cell.labUnit.text = model.strProduct_unit;
        
        return cell;
    }else{
        static NSString *adIfier = @"EditCartTableViewCell" ;
        EditCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:adIfier];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"EditCartTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        GoodsModel *model = [self.arrCartList objectAtIndex:indexPath.row];
        cell.btnSelect.selected = model.isSelected;
        cell.btnSelect.tag = indexPath.row + 1000;
        [cell.btnSelect addTarget:self action:@selector(editOrderClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.imgMain setImageWithURL:[NSURL URLWithString:model.s_image_url] placeholderImage:IMAGE(@"defaultPic")];
        cell.labTitle.text = model.title;
        cell.txtCount.text = model.goods_num;
        NSArray *arr = [model.totalPrice componentsSeparatedByString:@"."];
        cell.labPriceStar.text = [arr objectAtIndex:0];
        if (arr.count>1) {
            cell.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arr objectAtIndex:1]];
        }
//        cell.labUnit.text = model.strProduct_unit;
        cell.txtCount.delegate = self;
        cell.txtCount.tag = indexPath.row + 10000;
        return cell;
    }
}

-(void)addOrderClick:(UIButton *)sender{
    if (sender.selected) {
        GoodsModel *model = [self.arrCartList objectAtIndex:(sender.tag - 100)];
        model.isSelected = NO;
        [self.arrCartList replaceObjectAtIndex:(sender.tag - 100) withObject:model];
    }else{
        GoodsModel *model = [self.arrCartList objectAtIndex:(sender.tag - 100)];
        model.isSelected = YES;
        [self.arrCartList replaceObjectAtIndex:(sender.tag - 100) withObject:model];
    }
    sender.selected =  !sender.selected;
    
    
    BOOL isSelect = YES;
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == NO) {
            isSelect= NO;
            break;
        }
    }
   
    NSInteger count = 0;
    float price = 0.0;
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == YES) {
            count = count + [model.goods_num integerValue];
            price = price + [model.totalPrice floatValue];
        }
    }
    NSArray *arrPrice = [[NSString stringWithFormat:@"%.2f",price] componentsSeparatedByString:@"."];
    self.labPriceStar.text = [arrPrice objectAtIndex:0];
    if (arrPrice.count>1) {
        self.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arrPrice objectAtIndex:1]];
    }
    self.labCount.text = [NSString stringWithFormat:@"%zd",count];
    self.btnSelectAll1.selected = isSelect;
    self.btnSelectAll2.selected = isSelect;
    [self.tableView reloadData];
}

-(void)editOrderClick:(UIButton *)sender{
    if (sender.selected) {
        GoodsModel *model = [self.arrCartList objectAtIndex:(sender.tag - 1000)];
        model.isSelected = NO;
        [self.arrCartList replaceObjectAtIndex:(sender.tag - 1000) withObject:model];
    }else{
        GoodsModel *model = [self.arrCartList objectAtIndex:(sender.tag - 1000)];
        model.isSelected = YES;
        [self.arrCartList replaceObjectAtIndex:(sender.tag - 1000) withObject:model];
    }
    sender.selected =  !sender.selected;
    BOOL isSelect = YES;
    
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == NO) {
            isSelect= NO;
            break;
        }
    }
    NSInteger count = 0;
    float price = 0.0;
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == YES) {
            count = count + [model.goods_num integerValue];
            price = price + [model.totalPrice floatValue];
        }
    }
    NSArray *arrPrice = [[NSString stringWithFormat:@"%.2f",price] componentsSeparatedByString:@"."];
    self.labPriceStar.text = [arrPrice objectAtIndex:0];
    if (arrPrice.count>1) {
        self.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arrPrice objectAtIndex:1]];
    }
    self.labCount.text = [NSString stringWithFormat:@"%zd",count];
    self.btnSelectAll1.selected = isSelect;
    self.btnSelectAll2.selected = isSelect;
    
    [self.tableView reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     GoodsModel *model = [self.arrCartList objectAtIndex:indexPath.row];
    DetailViewController *vc=[[DetailViewController alloc]init];
    vc.strGood_id = model.deal_id;
    [self.tabBarController.navigationController pushViewController:vc animated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 106;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrCartList.count;
}

- (void)editClick1:(id)sender {
    
    if(self.btnEdit.selected == NO){
        self.btnEdit.selected = !self.btnEdit.selected;
        [self.tableView reloadData];
        
    }else{
        self.btnEdit.selected = !self.btnEdit.selected;
        [self.txtField resignFirstResponder];
        
        for (int i = 0; i< self.arrCartList.count; i++) {
            GoodsModel *model = [self.arrCartList objectAtIndex:i];
            [DBDaoHelper updateCartWith:model.deal_id goods_num:model.goods_num];
        }
        [self loadCartList];
        
    }
    self.deleteView.hidden = !self.deleteView.hidden;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.txtField = textField;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[0-9]*$" options:0 error:nil];
    if (regex != nil) {
        NSTextCheckingResult *firstMatch = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
        
        if (firstMatch) {
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger index = self.txtField.tag - 10000;
    GoodsModel *model = [self.arrCartList objectAtIndex:index];
    model.goods_num = self.txtField.text;
    [self.arrCartList replaceObjectAtIndex:index withObject:model];

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnDeleteClick:(id)sender {
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected) {
           [DBDaoHelper deleteCartWith:model.deal_id];
        }
        
    }
    [self loadCartList];
}

- (IBAction)selectAll:(UIButton *)sender {
    
    if (sender.selected) {
        for (int i = 0; i< self.arrCartList.count; i++) {
            GoodsModel *model = [self.arrCartList objectAtIndex:i];
            model.isSelected = NO;
            [self.arrCartList replaceObjectAtIndex:i withObject:model];
        }
    }else{
              for (int i = 0; i< self.arrCartList.count; i++) {
            GoodsModel *model = [self.arrCartList objectAtIndex:i];
            model.isSelected = YES;
            [self.arrCartList replaceObjectAtIndex:i withObject:model];
        }
    }
    NSInteger count = 0;
    float price = 0.0;
    for (int i = 0; i< self.arrCartList.count; i++) {
        GoodsModel *model = [self.arrCartList objectAtIndex:i];
        if (model.isSelected == YES) {
            count = count + [model.goods_num integerValue];
            price = price + [model.totalPrice floatValue];
        }
    }
    NSArray *arrPrice = [[NSString stringWithFormat:@"%.2f",price] componentsSeparatedByString:@"."];
    self.labPriceStar.text = [arrPrice objectAtIndex:0];
    if (arrPrice.count>1) {
        self.labPriceEnd.text = [NSString stringWithFormat:@".%@",[arrPrice objectAtIndex:1]];
    }

    self.labCount.text = [NSString stringWithFormat:@"%zd",count];
    [self.tableView reloadData];
    self.btnSelectAll1.selected = !sender.selected;
    self.btnSelectAll2.selected = !sender.selected;
}

- (IBAction)balanceClick:(id)sender {
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"userName"];
    if (userName.length ==0)
    {
        LoginVC *vc = [[LoginVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
        for (int i = 0; i<self.arrCartList.count; i++) {
            GoodsModel *goodModel = [self.arrCartList objectAtIndex:i];
            if (goodModel.isSelected == YES) {
                
                [arrTemp addObject:goodModel];
            }
        }
        ;
        if (arrTemp.count==0) {
            [[CommonUtils sharedCommonUtils]showAlert:@"select your goods" delegate:nil];
        }else{
            OrderConfirmViewController *vc = [[OrderConfirmViewController alloc]init];
            vc.allList = arrTemp;
            [self.tabBarController.navigationController pushViewController:vc animated:YES];
        }
       
    }

}

//pull for refresh
- (void)pullDownEvent{
    [self loadCartList];
}


@end
