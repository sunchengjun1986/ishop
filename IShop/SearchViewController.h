

#import "BaseViewController.h"
#import "PullToRefreshTableView.h"
@interface SearchViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,TablePullDownEvent,TablePullUpEvent,UISearchBarDelegate>
{
    int _page;
}
@property (retain, nonatomic) PullToRefreshTableView *tableView;
@property (retain, nonatomic) NSMutableArray *mutabelArr;
@property (retain, nonatomic) NSMutableArray *arrList;
@property (retain, nonatomic) NSString *strSearch;

@property(retain, nonatomic) UISearchBar *searchBar; 
@end
