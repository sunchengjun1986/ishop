
#import "GoodsListTableViewCell.h"

@implementation GoodsListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.strSerialNumber = @"";
        //fig
        self.imageViewIcon = [[UIImageView alloc]init];
        self.imageViewIcon.frame = CGRectMake(kWidth(7), kHeight(7), kWidth(90), kHeight(90));
        [self addSubview:self.imageViewIcon];
        
        //name
        self.labelName = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(107), kHeight(7), kWidth(197), kHeight(17))];

        self.labelName.font = [UIFont systemFontOfSize:15];
        self.labelName.textColor = UIColorFromRGB(0x333333);
        [self addSubview:self.labelName];
        
        //sample
        UILabel *labelSymbol = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(107), kHeight(39), kWidth(14), kHeight(14))];
        labelSymbol.text = @"￥";
        labelSymbol.font = [UIFont systemFontOfSize:18];
        labelSymbol.textColor = UIColorFromRGB(0xf4ad45);
        [self addSubview:labelSymbol];
        
        //currency
        self.labelPriceYuan = [[UILabel alloc]initWithFrame:CGRectMake(kWidth(122), kHeight(37), kWidth(200), kHeight(15))];
        self.labelPriceYuan.font = [UIFont systemFontOfSize:15];
        self.labelPriceYuan.textColor = UIColorFromRGB(0xf4ad45);
        [self addSubview:self.labelPriceYuan];
        

        
        //number
        self.textFiledNumber = [[UITextField alloc]initWithFrame:CGRectMake(107, kHeight(71), 55, kHeight(26))];
        self.textFiledNumber.font = [UIFont systemFontOfSize:12];
        self.textFiledNumber.text = @"expired:";
        self.textFiledNumber.userInteractionEnabled = NO;
        self.textFiledNumber.textColor = UIColorFromRGB(0x333333);
        self.textFiledNumber.returnKeyType = UIReturnKeyDone;
        self.textFiledNumber.keyboardType = UIKeyboardTypeNumberPad;
        self.textFiledNumber.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.textFiledNumber];
        
        //button
        self.btnDanwei = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnDanwei.frame = CGRectMake(kWidth(145), kHeight(70),kScreenWidth - kWidth(150)-10, kHeight(28));
        [self.btnDanwei setBackgroundImage:IMAGE(@"down_icon_nor") forState:UIControlStateNormal];
        [self.btnDanwei setBackgroundImage:IMAGE(@"down_icon_pres") forState:UIControlStateHighlighted];
        [self addSubview:self.btnDanwei];
        
        //text of unit
        self.labelDanwei = [[UILabel alloc]initWithFrame:CGRectMake(0, kHeight(6),kScreenWidth - kWidth(150)-10, kHeight(15))];
        self.labelDanwei.textAlignment = NSTextAlignmentCenter;
        self.labelDanwei.textColor = UIColorFromRGB(0x818590);
        self.labelDanwei.font = [UIFont systemFontOfSize:12];
        [self.btnDanwei addSubview:self.labelDanwei];
        

    }
    return self;
}


@end
